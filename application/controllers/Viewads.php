<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Viewads extends CI_Controller {

    public function index() {
        $this->load->view('page-adss');
    }

    public function view_by_catogary() {

        $this->load->model('ads_model');
        $cat = $this->input->post('catid');
        $result['adsab'] = $this->ads_model->ads_by_cat($cat);
        $this->load->view('page-ads', $result);
    }

    public function view_by_add() {

        $this->load->model('ads_model');
        $add = $this->input->post('add');
        echo json_encode($this->ads_model->ads_by_id($add));
    }

    public function view_by_add_page() {

        $this->load->model('ads_model');
        $add = $this->input->post('add');
        $result['adsab'] = $this->ads_model->ads_by_id($add);
        $this->load->view('add-detail', $result);
    }

    public function all_ads() {
        $this->load->model('ads_model');
        $result['adsab'] = $this->ads_model->all_ads();
        $this->load->view('page-ads', $result);
    }

    public function wanted_ads() {
        $this->load->model('ads_model');
        $result['wantedAds'] = $this->ads_model->wanted_ads();
        
        $this->load->view('page-wanted', $result);
    }

}
