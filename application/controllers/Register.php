<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
require_once 'vendor/mashape/unirest-php/src/Unirest.php';

class Register extends CI_Controller {

	public function index()
	{
		$this->load->view('page-register');
	}

	public function save_normal_user(){
		$this->load->model('User_model');
		session_start();

		$name = $this->input->post('name');
		$contact = $this->input->post('contact');
		$password = $this->input->post('password');
		$long = $this->input->post('long');
		$lat = $this->input->post('lat');
		$usertype=3;
		$telephone=str_replace("0","2B94",$contact);
		$telephones=str_replace("0","94",$contact);
		$t=time();
		$code=substr($t,3);
		$response=$this->User_model->save_user($name,$contact,$password,$usertype,$code,$long,$lat);
		$_SESSION["user"] = $contact;
                $_SESSION["userObject"] = $this->User_model->verify_user($contact);
		$msg="";
		if($response==true){
$url="http://world.msg91.com/api/otp.php?authkey=2970AaI66NN3yw5d3f1b81&mobile=".$telephones."&message=Your%20Verification%20Code%20is%20".$code."&sender=SMSIND&otp=".$code."&otp_length=4";
$ch = curl_init();
curl_setopt_array($ch, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_GET => true,
));
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$output = curl_exec($ch);
if(curl_errno($ch))
{
    // echo 'error:' . curl_error($ch);
}
curl_close($ch);
			$msg = "success";
		}else if($response==false){
			$msg="error";
		}
		echo json_encode($msg);
	}

	public function save_technician(){
		$this->load->model('User_model');
		session_start();

		$name = $this->input->post('name');
		$contact = $this->input->post('contact');
		$password = $this->input->post('password');
                $long = $this->input->post('long');
		$lat = $this->input->post('lat');
		$usertype=2;
		$telephone=str_replace("0","2B94",$contact);
		$t=time();
		$code=substr($t,3);
		$response=$this->User_model->save_user($name,$contact,$password,$usertype,$code,$long,$lat);
		$_SESSION["user"] = $contact;
                $_SESSION["userObject"] = $this->User_model->verify_user($contact);
		$msg="";
		if($response==true){
			$msg = Unirest\Request::post("https://telesign-telesign-send-sms-verification-code-v1.p.rapidapi.com/sms-verification-code?phoneNumber=%$telephone&verifyCode=$code",
            array(
            "X-RapidAPI-Host" => "telesign-telesign-send-sms-verification-code-v1.p.rapidapi.com",
            "X-RapidAPI-Key" => "4683aebfd7mshc9006b07df8b58dp1917dcjsn49a6e6f29813",
            "Content-Type" => "application/x-www-form-urlencoded"
            )
		   );
		}else if($response==false){
			$msg="error";
		}
		echo json_encode($msg);
	}

}
