<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index() {
        $this->load->model('Admin_model');
        $result['cat'] = $this->Admin_model->get_categories();
        $this->load->view('index', $result);
    }

}
