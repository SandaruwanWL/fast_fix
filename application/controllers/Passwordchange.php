<?php
/**
 *
 * @author manoj
 */
class Passwordchange extends CI_Controller {

    public function index() {
      $this->load->view('password-change');
    }
    
    public function changePassword() {
      $this->load->model('Admin_model');
      $pw = $this->input->post('pw');
      $new = $this->input->post('new');
      $confm = $this->input->post('confm');
      $this->Admin_model->changePassword($pw, $new, $confm);
    }
}
