<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        $this->load->view('page-login');
    }

    public function user_login() {
        $this->load->model('User_model');
        session_start();

        $contact = $this->input->post('contact');
        $password = $this->input->post('password');
        $response = $this->User_model->verify_user($contact);
        $msg = "";
        if (md5($password) == $response['password']) {
            $_SESSION["user"] = $contact;
            $_SESSION["userObject"] = $this->User_model->verify_user($contact);

            //////////////////////////////////////////////////////////////////////
//            $this->load->library('session');
//            $username = $this->User_model->verify_user($contact);
//            $userdata = array(
//                'contact' => $contact,
//                'username' => $username
//            );
//            $this->session->set_userdata($userContact);
            //////////////////////////////////////////////////////////////////////

            $msg = "success";
        } else if (md5($password) != $response['password']) {
            $msg = "error";
        }
        echo json_encode($msg);
    }

}
