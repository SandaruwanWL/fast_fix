<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {

	public function index()
	{
		$this->load->view('page-verify');
	}

	public function verify_user(){
        $this->load->model('User_model');
        session_start();

        $code = $this->input->post('code');
        $user=$_SESSION["user"];
        $response=$this->User_model->verify_user($user);
		$msg="";
		if($response['code']==$code){
            $this->User_model->update_user_verify($user);
            $msg="success";
		}else if($response['code']!=$code){
			$msg="error";
		}
		echo json_encode($msg);
	}

}
