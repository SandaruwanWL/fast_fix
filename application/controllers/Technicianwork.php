<?php

/**
 * @author manoj
 */
class Technicianwork extends CI_Controller {

    public function index() {
        $this->load->model('Admin_model');
        session_start();
        $current_technician=$_SESSION["userObject"]['contact'];

        $result['pendingjobs'] = $this->Admin_model->get_pending_jobs($current_technician);
        $result['approvedjobs'] = $this->Admin_model->get_aproved_jobs($current_technician);
        $result['completedjobs'] = $this->Admin_model->get_completed_jobs($current_technician);
        $result['rejectedjobs'] = $this->Admin_model->get_rejected_jobs($current_technician);
        $this->load->view('page-technicianwork',$result);
    }

}
