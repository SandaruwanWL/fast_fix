<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addmyservice extends CI_Controller {

	public function index()
	{
        // $this->load->model('Admin_model');
		// $result['dis']=$this->Admin_model->get_districts();
		// $result['city']=$this->Admin_model->get_cities();
        // $this->load->view('page-technicianprofile',$result);
        
        $this->load->view('page-technicianprofile');
    }
    
    public function update_user_details(){
        $this->load->model('User_model');

        $name = $this->input->post('name');
        $dob = $this->input->post('dob');
        $mobile = $this->input->post('mobile');
        $fixed = $this->input->post('fixed');
        $district = $this->input->post('district');
        $city = $this->input->post('city');
        $address = $this->input->post('address');

		$response=$this->User_model->update_user_details($name,$dob,$mobile,$fixed,$district,$city,$address);
		$msg="";
		if($response==1){
			$msg="success";
		}else if(!$response==0){
			$msg="error";
		}
		echo json_encode($msg);
	}

}
