<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index() {
        $this->load->view('page-dashboard');
    }

    public function gmap() {
        $this->load->view('gmap');
    }

    public function getAllAdsById($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_allAds_by_id($id));
    }

    public function savejob() {
        $this->load->model('Admin_model');
        $technician_id = $this->input->post('tech_id');
        $user_id = $this->input->post('u_id');
        $jobtitle = $this->input->post('jobtitle');
        $description = $this->input->post('jobdesc');
        $sms = $description . " (" . $user_id . ")";
        $encodedMessage = urlencode($sms);
        $added_date = date("Y-m-d");
        $resp = $this->Admin_model->savejob($technician_id, $user_id, $description, $added_date, $jobtitle);
        $msg = "";
        $phonenumber = substr($technician_id, 1);
        $tele = "94" . $phonenumber;
        if ($resp == 1) {
            $msg = "success";
            $url = "http://world.msg91.com/api/sendhttp.php?authkey=2970AaI66NN3yw5d3f1b81&mobiles=" . $tele . "&message=" . $encodedMessage . "&sender=SMSIND&route=4&country=0&flash=1&unicode=0";
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_GET => true
            ));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $output = curl_exec($ch);
            if (curl_errno($ch)) {
                // echo 'error:' . curl_error($ch);
            }
            curl_close($ch);
        } else if ($resp == 0) {
            $msg = "error";
        }
        echo json_encode($msg);
    }

    public function rejectjob() {
        $this->load->model('Admin_model');
        $jobid = $this->input->post('jobid');
        $resp = $this->Admin_model->rejectjob($jobid);
        if ($resp == 1) {
            $result['pendingjobs'] = $this->Admin_model->get_pending_jobs();
            $result['approvedjobs'] = $this->Admin_model->get_aproved_jobs();
            $result['completedjobs'] = $this->Admin_model->get_completed_jobs();
            $result['rejectedjobs'] = $this->Admin_model->get_rejected_jobs();
            $this->load->view('page-technicianwork', $result);
        } else if ($resp == 1) {
            echo "error = " . $resp;
        }
    }

    public function approvejob() {
        $this->load->model('Admin_model');
        $jobcost = $this->input->post('jobcost');
        $jbid = $this->input->post('jbid');
        $resp = $this->Admin_model->approvejob($jobcost, $jbid);
        if ($resp == 1) {
            $result['pendingjobs'] = $this->Admin_model->get_pending_jobs();
            $result['approvedjobs'] = $this->Admin_model->get_aproved_jobs();
            $result['completedjobs'] = $this->Admin_model->get_completed_jobs();
            $result['rejectedjobs'] = $this->Admin_model->get_rejected_jobs();
            $this->load->view('page-technicianwork', $result);
        } else if ($resp == 1) {
            echo "error = " . $resp;
        }
    }

    public function completejob() {
        $this->load->model('Admin_model');
        $jbid = $this->input->post('jobid');
        $resp = $this->Admin_model->completejob($jbid);
        if ($resp == 1) {
            $result['pendingjobs'] = $this->Admin_model->get_pending_jobs();
            $result['approvedjobs'] = $this->Admin_model->get_aproved_jobs();
            $result['completedjobs'] = $this->Admin_model->get_completed_jobs();
            $result['rejectedjobs'] = $this->Admin_model->get_rejected_jobs();
            $this->load->view('page-technicianwork', $result);
        } else if ($resp == 1) {
            echo "error = " . $resp;
        }
    }

    public function payjob() {
        $this->load->model('Admin_model');
        $jbid = $this->input->post('jbid');
        $resp = $this->Admin_model->payjob($jbid);
        if ($resp == 1) {
            session_start();
            $current_user = $_SESSION["userObject"]['contact'];
            $current_user_name = $_SESSION["userObject"]['name'];

//          ----------------------------SMS-------------------------------------
//            try {
//                $encodedMessage = urlencode($current_user_name . " Payed your job");
//                $url = "http://world.msg91.com/api/sendhttp.php?authkey=2970AaI66NN3yw5d3f1b81&mobiles=" . $current_user . "&message=" . $encodedMessage . "&sender=SMSIND&route=4&country=0&flash=1&unicode=0";
//                $ch = curl_init();
//                curl_setopt_array($ch, array(
//                    CURLOPT_URL => $url,
//                    CURLOPT_RETURNTRANSFER => true,
//                    CURLOPT_GET => true
//                ));
//                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//                $output = curl_exec($ch);
//                if (curl_errno($ch)) {
//                    // echo 'error:' . curl_error($ch);
//                }
//                curl_close($ch);
//            } catch (exception $e) {
//                print_r($e);
//            }
//          ----------------------------SMS-------------------------------------


            $result['pendingjobs'] = $this->Admin_model->get_pending_jobs_u($current_user);
            $result['approvedjobs'] = $this->Admin_model->get_aproved_jobs_u($current_user);
            $result['completedjobs'] = $this->Admin_model->get_completed_jobs_u($current_user);
            $result['rejectedjobs'] = $this->Admin_model->get_rejected_jobs_u($current_user);
            $this->load->view('page-user-jobs', $result);
        } else if ($resp == 1) {
            echo "error = " . $resp;
        }
    }

    public function get_user_details() {
        $this->load->model('User_model');
        session_start();

        $contact = $this->input->post('contact');
        $response = $this->User_model->verify_user($contact);
        $msg = "";
        if ($response['name']) {
            $msg = $response;
        } else if (!$response['name']) {
            $msg = "error";
        }
        echo json_encode($msg);
    }

    public function activetechnicians() {
        $this->load->model('Admin_model');

        $result['activetechnician'] = $this->Admin_model->activetechnicians();
//        print_r($this->Admin_model->activetechnicians());
        $this->load->view('page-activetechnicians', $result);
    }

    public function inactivetechnicians() {
        $this->load->model('Admin_model');

        $result['inactivetechnicians'] = $this->Admin_model->inactivetechnicians();

        $this->load->view('page-inactivetechnicians', $result);
    }

    public function bannedtechnicians() {
        $this->load->model('Admin_model');

        $result['bannedtechnicians'] = $this->Admin_model->bannedtechnicians();

        $this->load->view('page-bannedtechnicians', $result);
    }

    public function deactivatetech() {
        $this->load->model('Admin_model');

        $user = $this->input->post('user');
        $resp = $this->Admin_model->deactivatetech($user);
        if ($resp == 1) {
            $result['inactivetechnicians'] = $this->Admin_model->inactivetechnicians();
            $this->load->view('page-inactivetechnicians', $result);
        } else if ($resp == 0) {
            $result['activetechnician'] = $this->Admin_model->activetechnicians();
            $this->load->view('page-activetechnicians', $result);
        }
    }

    public function penndingtechads() {
        $this->load->model('Admin_model');
        $result['penndingtechads'] = $this->Admin_model->penndingtechads();
        $this->load->view('page-penndingtechads', $result);
    }

    public function penndingtechadsactive() {
        $this->load->model('Admin_model');
        $this->Admin_model->penndingtechadsactive();
    }

    public function penndingwantedads() {
        $this->load->model('Admin_model');
        $result['penndingwantedads'] = $this->Admin_model->penndingwantedads();
        $this->load->view('page-penndingwantedads', $result);
    }

    public function activatetech() {
        $this->load->model('Admin_model');

        $user = $this->input->post('user');
        $resp = $this->Admin_model->activatetech($user);
        if ($resp == 1) {
            $result['activetechnician'] = $this->Admin_model->activetechnicians();
            $this->load->view('page-activetechnicians', $result);
        } else if ($resp == 0) {
            $result['inactivetechnicians'] = $this->Admin_model->inactivetechnicians();
            $this->load->view('page-inactivetechnicians', $result);
        }
    }

    public function bantech() {
        $this->load->model('Admin_model');

        $user = $this->input->post('user');
        $resp = $this->Admin_model->bantech($user);
        if ($resp == 1) {
            $result['bannedtechnicians'] = $this->Admin_model->bannedtechnicians();
            $this->load->view('page-bannedtechnicians', $result);
        } else if ($resp == 0) {
            // $result['inactivetechnicians']=$this->Admin_model->inactivetechnicians();
            // $this->load->view('page-inactivetechnicians',$result);
        }
    }

    public function activeusers() {
        $this->load->model('Admin_model');

        $result['activeusers'] = $this->Admin_model->activeusers();
//        print_r($this->Admin_model->activeusers());
        $this->load->view('page-activeusers', $result);
    }

    public function inactiveusers() {
        $this->load->model('Admin_model');

        $result['inactiveusers'] = $this->Admin_model->inactiveusers();

        $this->load->view('page-inactiveusers', $result);
    }

    public function bannedusers() {
        $this->load->model('Admin_model');

        $result['bannedusers'] = $this->Admin_model->bannedusers();

        $this->load->view('page-bannedusers', $result);
    }

    public function deactivateuser() {
        $this->load->model('Admin_model');

        $user = $this->input->post('user');
        $resp = $this->Admin_model->deactivatetech($user);
        if ($resp == 1) {
            $result['inactiveusers'] = $this->Admin_model->inactiveusers();
            $this->load->view('page-inactiveusers', $result);
        } else if ($resp == 0) {
            $result['activeusers'] = $this->Admin_model->activeusers();
            $this->load->view('page-activeusers', $result);
        }
    }

    public function activateuser() {
        $this->load->model('Admin_model');

        $user = $this->input->post('user');
        $resp = $this->Admin_model->activatetech($user);
        if ($resp == 1) {
            $result['activeusers'] = $this->Admin_model->activeusers();
            $this->load->view('page-activeusers', $result);
        } else if ($resp == 0) {
            $result['inactiveusers'] = $this->Admin_model->inactiveusers();
            $this->load->view('page-inactiveusers', $result);
        }
    }

    public function banuser() {
        $this->load->model('Admin_model');

        $user = $this->input->post('user');
        $resp = $this->Admin_model->bantech($user);
        if ($resp == 1) {
            $result['bannedusers'] = $this->Admin_model->bannedusers();
            $this->load->view('page-bannedusers', $result);
        } else if ($resp == 0) {
            // $result['inactivetechnicians']=$this->Admin_model->inactivetechnicians();
            // $this->load->view('page-inactivetechnicians',$result);
        }
    }

    public function approvedtechads() {
        $this->load->model('Admin_model');

        $result['approvedtechads'] = $this->Admin_model->approvedtechads();
//        print_r($this->Admin_model->approvedtechads());
        $this->load->view('page-approvedtechads', $result);
    }

    public function rejectedtechads() {
        $this->load->model('Admin_model');

        $result['rejectedtechads'] = $this->Admin_model->rejectedtechads();

        $this->load->view('page-rejectedtechads', $result);
    }

    public function inactivetechads() {
        $this->load->model('Admin_model');

        $result['inactivetechads'] = $this->Admin_model->inactivetechads();

        $this->load->view('page-inactivetechads', $result);
    }

    public function approvedwantedads() {
        $this->load->model('Admin_model');

        $result['approvedwantedads'] = $this->Admin_model->approvedwantedads();

        $this->load->view('page-approvedwantedads', $result);
    }

    public function rejectedwantedads() {
        $this->load->model('Admin_model');

        $result['rejectedwantedads'] = $this->Admin_model->rejectedwantedads();

        $this->load->view('page-rejectedwantedads', $result);
    }

    public function inactivewantedads() {
        $this->load->model('Admin_model');

        $result['inactivewantedads'] = $this->Admin_model->inactivewantedads();

        $this->load->view('page-inactivewantedads', $result);
    }

    public function rejecttechad() {
        $this->load->model('Admin_model');

        $id = $this->input->post('add_id');
        $resp = $this->Admin_model->rejecttechad($id);
        if ($resp == 1) {
            $result['rejectedtechads'] = $this->Admin_model->rejectedtechads();
            $this->load->view('page-rejectedtechads', $result);
        } else if ($resp == 0) {
            $result['approvedtechads'] = $this->Admin_model->approvedtechads();
            $this->load->view('page-approvedtechads', $result);
        }
    }

    public function approvetechad() {
        $this->load->model('Admin_model');

        $id = $this->input->post('add_id');
        $resp = $this->Admin_model->approvetechad($id);
        if ($resp == 1) {
            $result['approvedtechads'] = $this->Admin_model->approvedtechads();
            $this->load->view('page-approvedtechads', $result);
        } else if ($resp == 0) {
            $result['rejectedtechads'] = $this->Admin_model->rejectedtechads();
            $this->load->view('page-rejectedtechads', $result);
        }
    }

    public function rejectwantedad() {
        $this->load->model('Admin_model');

        $id = $this->input->post('add_id');
        $resp = $this->Admin_model->rejectwantedad($id);
        if ($resp == 1) {
            $result['rejectedwantedads'] = $this->Admin_model->rejectedwantedads();
            $this->load->view('page-rejectedwantedads', $result);
        } else if ($resp == 0) {
            $result['approvedwantedads'] = $this->Admin_model->approvedwantedads();
            $this->load->view('page-approvedwantedads', $result);
        }
    }

    public function approvewantedad() {
        $this->load->model('Admin_model');

        $id = $this->input->post('add_id');
        $resp = $this->Admin_model->approvewantedad($id);
        if ($resp == 1) {
            $result['approvedwantedads'] = $this->Admin_model->approvedwantedads();
            $this->load->view('page-approvedwantedads', $result);
        } else if ($resp == 0) {
            $result['rejectedwantedads'] = $this->Admin_model->rejectedwantedads();
            $this->load->view('page-rejectedwantedads', $result);
        }
    }

    public function printAds() {
        $this->load->model('Admin_model');
        $data['paiedAds'] = $this->Admin_model->get_ads_by_paied();
        $this->load->view('print-payment-tech-ad', $data);
    }

}
