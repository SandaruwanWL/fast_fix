<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function index() {
        $this->load->model('Admin_model');

        $result['cat'] = $this->Admin_model->get_categories();
        $this->load->view('page-categories', $result);
    }

    public function getCategoryById($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_categorie_by_id($id));
    }

    public function save() {
        $this->load->model('Admin_model');
        $this->load->helper('url');

        $target_dir = "images/category-icons/";
        $target_file = $target_dir . basename($_FILES["myfile"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["myfile"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        if ($_FILES["myfile"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES["myfile"]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        $category = $this->input->post('category');
        $icon = $target_file;
        $response = $this->Admin_model->save_category($category, $icon);
        $msg = "";
        if ($response == true) {
            $msg = "success";
            redirect('categories');
        } else if ($response == false) {
            $msg = "error";
            redirect('categories?msg=' + $msg);
        }
    }

    public function update() {
        $this->load->model('Admin_model');
        $this->load->helper('url');

        $target_dir = "images/category-icons/";
        $target_file = $target_dir . basename($_FILES["myfile"]["name"]);
        if (basename($_FILES["myfile"]["name"])) {
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            if (isset($_POST["submit"])) {
                $check = getimagesize($_FILES["myfile"]["tmp_name"]);
                if ($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            if ($_FILES["myfile"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $target_file)) {
                    echo "The file " . basename($_FILES["myfile"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            $category = $this->input->post('category');
            $id = $this->input->post('catId');
            $icon = $target_file;
            $response = $this->Admin_model->update_category1($id, $category, $icon);
            $msg = "";
            if ($response == true) {
                $msg = "success";
                redirect('categories');
            } else if ($response == false) {
                $msg = "error";
                redirect('categories?msg=' + $msg);
            }
        } else {
            $category = $this->input->post('category');
            $id = $this->input->post('catId');
            $response = $this->Admin_model->update_category2($id, $category);
            $msg = "";
            if ($response == true) {
                $msg = "success";
                redirect('categories');
            } else if ($response == false) {
                $msg = "error";
                redirect('categories?msg=' + $msg);
            }
        }
    }
    
     public function deleteCategory($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->delete_category($id));
    }

}
