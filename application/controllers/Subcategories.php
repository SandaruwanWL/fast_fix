<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategories extends CI_Controller {

    public function index() {
        $this->load->model('Admin_model');

        $result['cat'] = $this->Admin_model->get_categories();
//        $result['chsc'] = $this->Admin_model->get_chsc();
        $this->load->view('page-subcategories', $result);
    }

    public function save() {
        $this->load->model('Admin_model');
        $this->load->helper('url');

        $category_id = $this->input->post('category');
        $subcategory = $this->input->post('subcategory');
        $this->Admin_model->save_subcategory($category_id, $subcategory);
        redirect('subcategories');
    }
    public function update() {
        $this->load->model('Admin_model');
        $this->load->helper('url');

        $subcategory_id = $this->input->post('subcategoryid');
        $subcategory = $this->input->post('subcategory');
        $this->Admin_model->update_subcategory($subcategory_id, $subcategory);
        redirect('subcategories');
    }
    
    public function deleteSubCategory($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->delete_sub_category($id));
    }
    
    public function getSubCategoryById($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_sub_category_by_id($id));
    }

}
