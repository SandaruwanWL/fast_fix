<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ads extends CI_Controller {

	public function index()
	{
		// $this->load->model('Admin_model');

		// $result['districts']=$this->Admin_model->get_districts();
		// $result['dhc']=$this->Admin_model->get_dhc();
		 $this->load->view('ads');
	}
	
	public function view_by_catogary()
	{

		$this->load->model('wanted_ads');
		$this->load->helper('url'); 
		$cat = $this->input->post('catid');
		

		$result['wanted']=$this->wanted_ads->wantedd_ads($cat);

		$this->load->view('ads',$result);
	}
	
    
}
