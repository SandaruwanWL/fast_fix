<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Districts extends CI_Controller {

	public function index()
	{
		$this->load->model('Admin_model');
		$result['data']=$this->Admin_model->get_districts();
		$this->load->view('page-districts',$result);
    }
	
	public function save(){
		$this->load->model('Admin_model');
		$this->load->helper('url'); 

		$district = $this->input->post('district');
		$response=$this->Admin_model->save_district($district);
		$msg="";
		if($response==true){
			$msg = "success";
			redirect('districts'); 
		}else if($response==false){
			$msg="error";
			redirect('districts?msg='+$msg); 
		}
	}
	
}
