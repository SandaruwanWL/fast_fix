<?php

/**
 * @author manoj
 */
class Technicianads extends CI_Controller {

    public function index() {
        $this->load->model('Admin_model');
        session_start();
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);

        $data['loginUser'] = $loginUser;
        $data['ads'] = $this->Admin_model->get_ads_by_technician($loginUser['contact']);
        $data['categorys'] = $this->Admin_model->get_catregory_by_technician($loginUser['id']);
        $data['districts'] = $this->Admin_model->get_district_by_technician($loginUser['id']);
        $data['cities'] = $this->Admin_model->get_district_by_technician($loginUser['id']);
        $this->load->view('page-technician-ads', $data);
    }

    public function changeCitys($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_citie_by_district($id));
    }

    public function getAddbyId($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_ads_by_id($id));
    }

    public function saveAdd() {
        $this->load->model('Admin_model');
        session_start();
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);

        $category = $this->input->post('category');
        $district = $this->input->post('district');
        $city = $this->input->post('city');
        $mobile = $this->input->post('mobile');
        $fixed = $this->input->post('fixed');
        $description = $this->input->post('description');
        $paymentType = $this->input->post('cash');
        $amount = $this->input->post('amount');

        $response = $this->Admin_model->save_add($category, $district, $city, $mobile, $fixed, $description, $loginUser['contact'], $paymentType, $amount);
        $msg = "";
        if ($response == 1) {
            $msg = "success";
            
            $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);
            $data['loginUser'] = $loginUser;
            $data['ads'] = $this->Admin_model->get_ads_by_technician($loginUser['contact']);
            $data['categorys'] = $this->Admin_model->get_catregory_by_technician($loginUser['id']);
            $data['districts'] = $this->Admin_model->get_district_by_technician($loginUser['id']);
            $data['cities'] = $this->Admin_model->get_district_by_technician($loginUser['id']);
            $this->load->view('page-technician-ads', $data);
            
            
        } else if (!$response == 0) {
            $msg = "error";
        }
        echo json_encode($msg);
    }

}
