<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cities extends CI_Controller {

	public function index()
	{
		$this->load->model('Admin_model');

		$result['districts']=$this->Admin_model->get_districts();
		$result['dhc']=$this->Admin_model->get_dhc();
		$this->load->view('page-cities',$result);
	}
	
	public function save(){
		$this->load->model('Admin_model');
		$this->load->helper('url'); 

		$district_id = $this->input->post('district');
		$city = $this->input->post('city');
		$city_id=$this->Admin_model->save_city($city);
		$response=$this->Admin_model->save_district_has_city($district_id,$city_id);
		$msg="";
		if($response==true){
			$msg = "success";
			redirect('cities'); 
		}else if($response==false){
			$msg="error";
			redirect('cities?msg='+$msg); 
		}
	}
    
}
