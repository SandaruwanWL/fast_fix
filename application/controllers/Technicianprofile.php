<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Technicianprofile extends CI_Controller {

    public function index() {
        $this->load->model('Admin_model');
        $this->load->model('ads_model');
        $result['districts'] = $this->Admin_model->get_districts();
         session_start();
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);
//        $result['citys'] = $this->Admin_model->get_citie_by_saved_district($loginUser['district_id']);
        $result['cities'] = $this->ads_model->get_cities();
        $result['districtId'] = $loginUser['district_id'];
        $result['cityId'] = $loginUser['city_id'];
        $this->load->view('page-technicianprofile', $result);
    }

    public function changeCitys($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_citie_by_district($id));
    }
    
    public function getAdById($id) {
        $this->load->model('Admin_model');
        
//        echo json_encode($this->Admin_model->get_citie_by_district($id));
        echo json_encode($id);
    }

    public function updateUserDetails() {
        $this->load->model('User_model');
        $name = $this->input->post('name');
        $mobile = $this->input->post('usermobile');
        $fixed = $this->input->post('fixed');
        $district = $this->input->post('district');
        $city = $this->input->post('city');
        $address = $this->input->post('address');

        $this->User_model->update_user_details($name, $mobile, $fixed, $district, $city, $address);

        $this->load->helper('url');
        redirect('technician-profile');
    }

}
