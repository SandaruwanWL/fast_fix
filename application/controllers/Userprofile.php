<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile extends CI_Controller {

    public function index() {
        $this->load->model('ads_model');
        session_start();
        $result['catogaries'] = $this->ads_model->get_categories();
        $result['districts'] = $this->ads_model->get_districts();
        $result['cities'] = $this->ads_model->get_cities();
        $this->load->view('page-user', $result);
    }

    public function pageUserWantedAds() {
        $this->load->model('ads_model');
        session_start();
        $result['ads'] = $this->ads_model->wanted_ads_by_user($_SESSION["userObject"]['contact']);
        $result['catogaries'] = $this->ads_model->get_categories();
        $result['districts'] = $this->ads_model->get_districts();
        $result['cities'] = $this->ads_model->get_cities();
        $this->load->view('page-user-wanted-ads', $result);
    }

    public function userworks(){
        $this->load->model('Admin_model');
        session_start();
        $current_user=$_SESSION["userObject"]['contact'];

        $result['pendingjobs'] = $this->Admin_model->get_pending_jobs_u($current_user);
        $result['approvedjobs'] = $this->Admin_model->get_aproved_jobs_u($current_user);
        $result['completedjobs'] = $this->Admin_model->get_completed_jobs_u($current_user);
        $result['rejectedjobs'] = $this->Admin_model->get_rejected_jobs_u($current_user);
        $this->load->view('page-user-jobs', $result);
    }

    public function getloginUser() {
        $this->load->model('Admin_model');
        session_start();
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);
        echo json_encode($loginUser);
    }

    public function updateUserProfile() {
        $this->load->model('user_model');
        $this->load->helper('url');
        
        $mobile = $this->input->post('user');
        $name = $this->input->post('name');
        $district = $this->input->post('district');
        $city = $this->input->post('city');
        $address = $this->input->post('address');
        $this->user_model->update_user_details2($name, $mobile,$district, $city, $address);
        
        $this->load->model('ads_model');
        session_start();
        $result['catogaries'] = $this->ads_model->get_categories();
        $result['districts'] = $this->ads_model->get_districts();
        $result['cities'] = $this->ads_model->get_cities();
        redirect('Userprofile', $result);
    }

    public function saveWantedAds() {
        $this->load->model('ads_model');
        $this->load->helper('url');

        $category = $this->input->post('cat');
        $topic = $this->input->post('topic');
        $user = $this->input->post('user');
        $city = $this->input->post('city');
        $district = $this->input->post('dis');
        $description = $this->input->post('des');
        $con_1 = $this->input->post('c1');

        $this->ads_model->save_wanted_ads($topic, $category, $user, $city, $district, $description, $con_1);

        session_start();
        $result['ads'] = $this->ads_model->wanted_ads_by_user($_SESSION["userObject"]['contact']);
        $result['catogaries'] = $this->ads_model->get_categories();
        $result['districts'] = $this->ads_model->get_districts();
        $result['cities'] = $this->ads_model->get_cities();
        redirect('userwantedads', $result);
    }

}
