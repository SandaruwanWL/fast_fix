<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Technicianservice extends CI_Controller {

    public function index() {

        $this->load->model('Admin_model');
        session_start();
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);

        $result['services'] = $this->Admin_model->get_services();
        $result['catregory'] = $this->Admin_model->get_catregory_by_technician($loginUser['id']);
        $result['districts'] = $this->Admin_model->get_districts();
        $result['district'] = $this->Admin_model->get_district_by_technician($_SESSION["userObject"]['id']);
        $result['registedworkingcitis'] = $this->Admin_model->get_registedworkingcitis_by_technician($_SESSION["userObject"]['id']);
        $result['savedserviceotherinfo'] = $this->Admin_model->get_saved_service_other_info_by_technician($_SESSION["userObject"]['id']);
        $this->load->view('page-technicianservice', $result);
    }

    public function loadSubcategory($id) {

        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_sub_category_by_category($id));
    }

    public function saveSubCategory() {
        $subCategorys = $this->input->post('subCategorys');
        session_start();
        $this->load->model('Admin_model');
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);
        echo json_encode($this->Admin_model->save_sub_category_for_technician($subCategorys, $loginUser));
    }

    public function citysBySelectedDistrics($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_citie_by_saved_district($id));
    }

    public function getDistrictById($id) {
        $this->load->model('Admin_model');
        echo json_encode($this->Admin_model->get_district_by_id($id));
    }

    public function saveServiceDetails() {
        $this->load->model('Admin_model');
         session_start();
        $loginUser = $this->Admin_model->get_login_user($_SESSION["userObject"]['contact']);
        
        echo $this->Admin_model->save_service_details($this, $loginUser);
    }

}
