<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['login'] = 'login';
$route['logout'] = 'logout';
$route['register'] = 'register';
$route['verify'] = 'verify';

$route['dashboard'] = 'dashboard';
$route['categories'] = 'categories';
$route['subcategories'] = 'subcategories';
$route['districts'] = 'districts';
$route['cities'] = 'cities';

$route['technician-profile'] = 'technicianprofile';
$route['technician-service'] = 'technicianservice';
$route['technician-work'] = 'technicianwork';
$route['technician-ads'] = 'technicianads';
$route['add-my-service'] = 'addmyservice';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['activetechnicians'] = 'dashboard/activetechnicians';
$route['inactivetechnicians'] = 'dashboard/inactivetechnicians';
$route['bannedtechnicians'] = 'dashboard/bannedtechnicians';

$route['activeusers'] = 'dashboard/activeusers';
$route['inactiveusers'] = 'dashboard/inactiveusers';
$route['bannedusers'] = 'dashboard/bannedusers';

$route['deactivatetech'] = 'dashboard/deactivatetech';
$route['bantech'] = 'dashboard/bantech';
$route['activatetech'] = 'dashboard/activatetech';

$route['deactivateuser'] = 'dashboard/deactivateuser';
$route['banuser'] = 'dashboard/banuser';
$route['activateuser'] = 'dashboard/activateuser';

$route['penndingtechads'] = 'dashboard/penndingtechads';
$route['penndingtechadsactive'] = 'dashboard/penndingtechadsactive';
$route['approvedtechads'] = 'dashboard/approvedtechads';
$route['rejectedtechads'] = 'dashboard/rejectedtechads';
$route['inactivetechads'] = 'dashboard/inactivetechads';

$route['rejecttechad'] = 'dashboard/rejecttechad';
$route['approvetechad'] = 'dashboard/approvetechad';

$route['penndingwantedads'] = 'dashboard/penndingwantedads';
$route['approvedwantedads'] = 'dashboard/approvedwantedads';
$route['rejectedwantedads'] = 'dashboard/rejectedwantedads';
$route['inactivewantedads'] = 'dashboard/inactivewantedads';

$route['rejectwantedad'] = 'dashboard/rejectwantedad';
$route['approvewantedad'] = 'dashboard/approvewantedad';

$route['gmap'] = 'dashboard/gmap';
$route['categoryads'] = 'viewads/view_by_catogary';
$route['ad'] = 'viewads/view_by_add';
$route['adpage'] = 'viewads/view_by_add_page';
$route['allAds'] = 'Viewads/all_ads';
$route['wanted'] = 'Viewads/wanted_ads';

$route['techaddsave'] = 'technicianads/saveAdd';
$route['printAds'] = 'dashboard/printAds';

$route['password-change'] = 'Passwordchange';
$route['userwantedads'] = 'Userprofile/pageUserWantedAds';
$route['userworks'] = 'Userprofile/userworks';

$route['savejob'] = 'dashboard/savejob';
$route['rejectjob'] = 'dashboard/rejectjob';
$route['approvejob'] = 'dashboard/approvejob';
$route['completejob'] = 'dashboard/completejob';

$route['payjob'] = 'dashboard/payjob';




