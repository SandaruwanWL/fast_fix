<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wanted_ads extends CI_Model {

    public function save_user($name,$contact,$password,$usertype,$code){
        $this->load->database(); 

       
    }
    public function get_districts(){
        $this->load->database(); 

        $query = $this->db->get('district');
        return $query->result();
    }

    public function get_cities(){
        $this->load->database(); 

        $query = $this->db->get('city');
        return $query->result();
    }

    public function get_categories(){
        $this->load->database(); 

        $query = $this->db->get('category');
        return $query->result();
    }

    public function wantedd_ads($cat){
        $this->load->database(); 

        $this->db->where('catogary_id', $cat);
        $query = $this->db->get('wanted_ads');
        
        return $query->result();
    }


    public function save_ads($category,$user,$city,$district,$description,$contact_1,$contact_2,$icon){
        $this->load->database(); 

        $data = array(
        'catogary_id' => $category,
        'user_id' =>$user,
        'location' => $city,
        'contact_1' => $contact_1,
        'contact_2' => $contact_2,
        'description' => $description,
        'date_created' => date('y-m-d'),
        'district' => $district,
        'icon' => $icon,
        );

        $resp=$this->db->insert('wanted_ads', $data);
        return $resp;
    }

}