<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {

    public function save_user($name,$contact,$password,$usertype,$code,$long,$lat){
        $this->load->database(); 

        $data = array(
        'name' => $name,
        'contact' => $contact,
        'user_type_id' => $usertype,
        'password' => md5($password),
        'code' => $code,
        'longitude' => $long,
        'latitude' => $lat
        );

        $resp=$this->db->insert('user', $data);
        return $resp;
    }

    public function verify_user($user){
        $this->load->database(); 

        $this->db->where('contact',$user);
        $query=$this->db->get('user');
        $result=$query->row_array();
        return $result;
    }

    public function update_user_verify($user){
        $this->load->database(); 

        $this->db->set('verified', 1);
        $this->db->set('status', 1);
        $this->db->where('contact', $user);
        $resp=$this->db->update('user');
        return $resp;
    }

    public function update_user_details($name,$mobile,$fixed,$district,$city,$address){
        $this->load->database(); 
        $this->db->set('name', $name);
        $this->db->set('fixedline', $fixed);
        $this->db->set('district_id', $district);
        $this->db->set('city_id', $city);
        $this->db->set('address', $address);
        $this->db->where('contact', $mobile);
        $resp=$this->db->update('user');
        return $resp;
    }
    
    public function update_user_details2($name, $mobile,$district, $city, $address){
        $this->load->database(); 
        $this->db->set('name', $name);
        $this->db->set('district_id', $district);
        $this->db->set('city_id', $city);
        $this->db->set('address', $address);
        $this->db->where('contact', $mobile);
        $resp=$this->db->update('user');
        return $resp;
    }

}
