<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ads_model extends CI_Model {

    public function save_user($name, $contact, $password, $usertype, $code) {
        $this->load->database();
    }

    public function get_districts() {
        $this->load->database();
        $query = $this->db->get('districts');
        return $query->result();
    }

    public function get_cities() {
        $this->load->database();

        $query = $this->db->get('cities');
        return $query->result();
    }

    public function get_categories() {
        $this->load->database();

        $query = $this->db->get('category');
        return $query->result();
    }

    public function ads_by_cat($cat) {
        $this->load->database();

        $this->db->select('all_ads.contact_1 as contone,all_ads.contact_2 as conttwo,all_ads.description as desc,all_ads.add_id as addid,user.name as username,category.category as cate,'
                . 'districts.name_en as dist,cities.name_en as citys');
        $this->db->from('all_ads');
        $this->db->join('user', 'all_ads.user_id = user.contact');
        $this->db->join('category', 'all_ads.catogary_id = category.id');
        $this->db->join('cities', 'all_ads.location = cities.id');
        $this->db->join('districts', 'all_ads.district = districts.id');
        $this->db->where('all_ads.catogary_id',$cat);
        $this->db->where('all_ads.add_type','Technician');
        $this->db->where('all_ads.status',1);
        $query = $this->db->get();
        return $query->result();
    }

    public function ads_by_id($add) {
        $this->load->database();

        $this->db->select('all_ads.user_id as techids,all_ads.contact_1 as contone,all_ads.contact_2 as conttwo,all_ads.description as desc,all_ads.add_id as addid,user.name as username,category.category as cate,'
                . 'districts.name_en as dist,cities.name_en as citys');
        $this->db->from('all_ads');
        $this->db->join('user', 'all_ads.user_id = user.contact');
        $this->db->join('category', 'all_ads.catogary_id = category.id');
        $this->db->join('cities', 'all_ads.location = cities.id');
        $this->db->join('districts', 'all_ads.district = districts.id');

        $this->db->where("all_ads.add_id = '$add'");
        $query = $this->db->get();

        return $query->result();
    }

    public function all_ads() {
        $this->load->database();

        $this->db->select('all_ads.contact_1 as contone,all_ads.contact_2 as conttwo,all_ads.description as desc,all_ads.add_id as addid,user.name as username,category.category as cate,'
                . 'districts.name_en as dist,cities.name_en as citys');
        $this->db->from('all_ads');
        $this->db->join('user', 'all_ads.user_id = user.contact');
        $this->db->join('category', 'all_ads.catogary_id = category.id');
        $this->db->join('cities', 'all_ads.location = cities.id');
        $this->db->join('districts', 'all_ads.district = districts.id');

        $query = $this->db->get();

        return $query->result();
    }

    public function wanted_ads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.add_type','wanted');
        $this->db->where('all_ads.status',1);
        $this->db->join('user', 'all_ads.user_id = user.contact');
        $this->db->join('category', 'all_ads.catogary_id = category.id');
        $this->db->join('cities', 'all_ads.location = cities.id');
        $this->db->join('districts', 'all_ads.district = districts.id');
        $query = $this->db->get();

        return $query->result();
    }
    public function wanted_ads_by_user($contact) {
        $this->load->database();
        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 1);
        $this->db->where('all_ads.user_id', $contact);
        $this->db->where('all_ads.add_type', 'wanted');
        $this->db->join('user', 'all_ads.user_id = user.contact');
        $this->db->join('category', 'all_ads.catogary_id = category.id');
        $this->db->join('districts', 'all_ads.district = districts.id');
       
        $query = $this->db->get();
        return $query->result();
    }

    public function save_wanted_ads($topic, $category, $user, $city, $district, $description,$con_1) {
        $this->load->database();

        $data = array(
            'catogary_id' => $category,
            'user_id' => $user,
            'topic' => $topic,
            'location' => $city,
            'contact_1' => $con_1,
            'description' => $description,
            'date_created' => date('y-m-d'),
            'district' => $district,
            'add_type' => 'wanted',
            'status' => '0'
        );

        $resp = $this->db->insert('all_ads', $data);
        return $resp;
    }

}
