<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function save_district($district) {
        $this->load->database();

        $data = array(
            'district' => $district
        );

        $resp = $this->db->insert('district', $data);
        return $resp;
    }

    public function save_category($category, $icon) {
        $this->load->database();

        $data = array(
            'category' => $category,
            'icon' => $icon,
            'status' => 1
        );

        $resp = $this->db->insert('category', $data);
        return $resp;
    }

    public function changePassword($pw, $new, $confm) {

        return $resp;
    }

    public function update_category1($id, $category, $icon) {
        $this->load->database();

        $data = array(
            'category' => $category,
            'icon' => $icon,
            'status' => 1
        );
        $this->db->where('id', $id);
        $resp = $this->db->update('category', $data);
        return $resp;
    }

    public function update_category2($id, $category) {
        $this->load->database();

        $data = array(
            'category' => $category,
            'status' => 1
        );
        $this->db->where('id', $id);
        $resp = $this->db->update('category', $data);
        return $resp;
    }

    public function delete_category($id) {
        $this->load->database();

        $data = array(
            'status' => 0
        );
        $this->db->where('id', $id);
        $resp = $this->db->update('category', $data);
        return $resp;
    }

    public function save_district_has_city($district_id, $city_id) {
        $this->load->database();

        $data = array(
            'district_id' => $district_id,
            'city_id' => $city_id
        );

        $resp = $this->db->insert('district_has_city', $data);
        return $resp;
    }

    public function save_city($city) {
        $this->load->database();

        $data = array(
            'city' => $city
        );

        $this->db->insert('city', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function save_subcategory($category_id, $subcategory) {
        $this->load->database();
        $data = array(
            'sub_category' => $subcategory,
            'category_id' => $category_id
        );
        return $this->db->insert('sub_category', $data);
    }

    public function savejob($technician_id, $user_id, $description, $added_date, $jobtitle) {
        $this->load->database();
        $data = array(
            'technician_id' => $technician_id,
            'user_id' => $user_id,
            'job_title' => $jobtitle,
            'description' => $description,
            'added_date' => $added_date
        );
        return $this->db->insert('job', $data);
    }

    public function update_subcategory($subcategory_id, $subcategory) {
        $this->load->database();

        $data = array(
            'sub_category' => $subcategory,
            'status' => 1
        );
        $this->db->where('id', $subcategory_id);
        $resp = $this->db->update('sub_category', $data);
        return $resp;
    }

    public function rejectjob($jobid) {
        $this->load->database();

        $data = array(
            'status' => 3
        );
        $this->db->where('id', $jobid);
        $resp = $this->db->update('job', $data);
        return $resp;
    }

    public function approvejob($jobcost, $jbid) {
        $this->load->database();

        $data = array(
            'status' => 1,
            'price' => $jobcost
        );
        $this->db->where('id', $jbid);
        $resp = $this->db->update('job', $data);
        return $resp;
    }

    public function completejob($jbid) {
        $this->load->database();

        $data = array(
            'status' => 2
        );
        $this->db->where('id', $jbid);
        $resp = $this->db->update('job', $data);
        return $resp;
    }

    public function payjob($jbid) {
        $this->load->database();

        $data = array(
            'status' => 1,
            'paid' => 1
        );
        $this->db->where('id', $jbid);
        $resp = $this->db->update('job', $data);
        return $resp;
    }

    public function get_districts() {
        $this->load->database();

        $query = $this->db->get('districts');
        return $query->result();
    }

    public function get_citie_by_saved_district($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('district_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pending_jobs($current_technician) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 0);
        $this->db->where('technician_id', $current_technician);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_aproved_jobs($current_technician) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 1);
        $this->db->where('technician_id', $current_technician);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_completed_jobs($current_technician) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 2);
        $this->db->where('technician_id', $current_technician);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_rejected_jobs($current_technician) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 3);
        $this->db->where('technician_id', $current_technician);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pending_jobs_u($current_user) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 0);
        $this->db->where('user_id', $current_user);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_aproved_jobs_u($current_user) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 1);
        $this->db->where('user_id', $current_user);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_completed_jobs_u($current_user) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 2);
        $this->db->where('user_id', $current_user);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_rejected_jobs_u($current_user) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('job');
        $this->db->where('status', 3);
        $this->db->where('user_id', $current_user);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_login_user($id) {
        $this->load->database();


        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('contact', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_district_by_technician($id) {
        $this->load->database();


        $this->db->select('*');
        $this->db->from('user_has_district_has_city');
        $this->db->where('user_id', $id);
        $this->db->join('districts', 'districts.id = user_has_district_has_city.district_id');
        $this->db->group_by('user_has_district_has_city.district_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_registedworkingcitis_by_technician($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('user_has_district_has_city');
        $this->db->where('user_id', $id);
        $this->db->join('cities', 'cities.id = user_has_district_has_city.city_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_saved_service_other_info_by_technician($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('service');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_citie_by_district($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('district_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_categories() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_categorie_by_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_dhc() {
        $this->load->database();

        $this->db->select('city.city as city,district.district as district,district_has_city.id as id');
        $this->db->from('district_has_city');
        $this->db->join('city', 'city.id = district_has_city.city_id');
        $this->db->join('district', 'district.id = district_has_city.district_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_chsc() {
        $this->load->database();

//        $this->db->select('category.category as category,sub_category.sub_category as sub_category,sub_category.id as id');
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('sub_category.status', 1);
        $this->db->join('category', 'category.id = sub_category.category_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_sub_category_by_category_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('category_id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_sub_category_by_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function delete_sub_category($id) {
        $this->load->database();

        $data = array(
            'status' => 0
        );
        $this->db->where('id', $id);
        $resp = $this->db->update('sub_category', $data);
        return $resp;
    }

    public function get_services() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('category');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_sub_category_by_category($id) {
        $this->load->database();

        $this->db->select('sub_category.id as id, sub_category.sub_category as sub_category, category.category as category');
        $this->db->from('sub_category');
        $this->db->where('sub_category.category_id', $id);
        $this->db->where('sub_category.status', 1);
        $this->db->join('category', 'sub_category.category_id = category.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function save_sub_category_for_technician($subCategorys, $loginUser) {
        $this->load->database();

        $responce = "";

        foreach ($subCategorys as $value) {
            $data = array(
                'user_id' => $loginUser['id'],
                'sub_category_id' => $value
            );
            $responce = $this->db->insert('user_has_category', $data);
        }

        return $responce;
    }

    public function get_catregory_by_technician($id) {
        $this->load->database();

        $query = $this->db->query("SELECT * FROM category WHERE status='1' AND id IN(SELECT category_id FROM sub_category WHERE id IN(SELECT sub_category_id FROM user_has_category WHERE user_id='+$id+'))");
        return $query->result();
    }

    public function get_subCatregory_by_technician($userID, $categoryID) {
        $this->load->database();

        $query = $this->db->query("SELECT * FROM sub_category WHERE id IN(SELECT sub_category_id FROM user_has_category WHERE user_id='+$userID+') AND category_id='+$categoryID+'");
        return $query->result();
    }

    public function get_district_by_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('districts');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_city_by_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_working_day_by_name($name) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('week');
        $this->db->where('name', $name);
        $query = $this->db->get();
        return $query->result();
    }

    public function save_service_details($object, $loginUser) {
        $this->load->database();
        $responce = '';
//------------------------------------------------------------------------------       
        $citys = $object->input->post('citycheckbox');
        foreach ($citys as $city) {
            $data = array(
                'user_id' => $loginUser['id'],
                'district_id' => $this->get_city_by_id($city)[0]->district_id,
                'city_id' => $city
            );
            $responce = $this->db->insert('user_has_district_has_city', $data);
        }
//------------------------------------------------------------------------------
        $data = array(
            'user_id' => $loginUser['id'],
            'qualifications' => $object->input->post('qualifications'),
            'experiance' => $object->input->post('experiance'),
            'working_hours' => $object->input->post('workinghours'),
            'best_time_to_call' => $object->input->post('besttimetocall'),
            'Description' => $object->input->post('Description')
        );
        $responce = $this->db->insert('service', $data);
//------------------------------------------------------------------------------
        $workingdays = $object->input->post('workingdays');
        foreach ($workingdays as $workingday) {
            $data = array(
                'week_id' => $this->get_working_day_by_name($workingday)[0]->id,
                'user_id' => $loginUser['id']
            );
            $responce = $this->db->insert('working_days', $data);
        }
        return $responce;
    }

    public function activetechnicians() {
        $this->load->database();

//        $this->db->select('user.id as id,user.name as name,user.contact as contact,districts.name_en as district,cities.name_en as city');
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_type_id', 2);
        $this->db->where('status', 1);
//        $this->db->join('districts', 'user.district_id = districts.id');
//        $this->db->join('cities', 'user.city_id = cities.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function inactivetechnicians() {
        $this->load->database();

//        $this->db->select('user.id as id,user.name as name,user.contact as contact,districts.name_en as district,cities.name_en as city');
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_type_id', 2);
        $this->db->where('status', 0);
//        $this->db->join('districts', 'user.district_id = districts.id');
//        $this->db->join('cities', 'user.city_id = cities.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function bannedtechnicians() {
        $this->load->database();

//        $this->db->select('user.id as id,user.name as name,user.contact as contact,districts.name_en as district,cities.name_en as city');
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_type_id', 2);
        $this->db->where('status', 2);
//        $this->db->join('districts', 'user.district_id = districts.id');
//        $this->db->join('cities', 'user.city_id = cities.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function deactivatetech($user) {
        $this->load->database();

        $this->db->set('verified', 1);
        $this->db->set('status', 0);
        $this->db->where('contact', $user);
        $resp = $this->db->update('user');
        return $resp;
    }

    public function activatetech($user) {
        $this->load->database();

        $this->db->set('verified', 1);
        $this->db->set('status', 1);
        $this->db->where('contact', $user);
        $resp = $this->db->update('user');
        return $resp;
    }

    public function bantech($user) {
        $this->load->database();

        $this->db->set('verified', 1);
        $this->db->set('status', 2);
        $this->db->where('contact', $user);
        $resp = $this->db->update('user');
        return $resp;
    }

    public function activeusers() {
        $this->load->database();

//        $this->db->select('user.id as id,user.name as name,user.contact as contact,districts.name_en as district,cities.name_en as city');
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_type_id', 3);
        $this->db->where('status', 1);
//        $this->db->join('districts', 'user.district_id = districts.id');
//        $this->db->join('cities', 'user.city_id = cities.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function inactiveusers() {
        $this->load->database();

//        $this->db->select('user.id as id,user.name as name,user.contact as contact,districts.name_en as district,cities.name_en as city');
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_type_id', 3);
        $this->db->where('status', 0);
//        $this->db->join('districts', 'user.district_id = districts.id');
//        $this->db->join('cities', 'user.city_id = cities.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function bannedusers() {
        $this->load->database();

//        $this->db->select('user.id as id,user.name as name,user.contact as contact,districts.name_en as district,cities.name_en as city');
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_type_id', 3);
        $this->db->where('status', 2);
//        $this->db->join('districts', 'user.district_id = districts.id');
//        $this->db->join('cities', 'user.city_id = cities.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function penndingtechads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 0);
        $this->db->where('all_ads.add_type', 'Technician');
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function penndingwantedads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 0);
        $this->db->where('all_ads.add_type', 'wanted');
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function approvedtechads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 1);
        $this->db->where('all_ads.add_type', 'Technician');
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function rejectedtechads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 2);
        $this->db->where('all_ads.add_type', 'technician');
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function inactivetechads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 0);
        $this->db->where('all_ads.add_type', 'technician');
        $this->db->join('districts', 'districts.id = all_ads.location');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function approvedwantedads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 1);
        $this->db->where('all_ads.add_type', 'wanted');
        $this->db->join('districts', 'districts.id = all_ads.location');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_ads_by_technician($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 1);
        $this->db->where('all_ads.user_id', $id);
        $this->db->where('all_ads.add_type', 'Technician');
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function rejectedwantedads() {
        $this->load->database();
        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 2);
        $this->db->where('all_ads.add_type', 'wanted');
        $this->db->join('districts', 'districts.id = all_ads.location');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_ads_by_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 2);
        $this->db->where('all_ads.add_type', 'wanted');
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $this->db->where('add_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_allAds_by_id($id) {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.add_id', $id);
        $this->db->join('districts', 'districts.id = all_ads.district');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $this->db->join('user', 'user.contact = all_ads.user_id');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function inactivewantedads() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 0);
        $this->db->where('all_ads.add_type', 'wanted');
        $this->db->join('districts', 'districts.id = all_ads.location');
        $this->db->join('category', 'category.id = all_ads.catogary_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function approvetechad($id) {
        $this->load->database();

        $this->db->set('status', 1);
        $this->db->where('add_id', $id);
        $resp = $this->db->update('all_ads');
        return $resp;
    }

    public function rejecttechad($id) {
        $this->load->database();

        $this->db->set('status', 2);
        $this->db->where('add_id', $id);
        $resp = $this->db->update('all_ads');
        return $resp;
    }

    public function approvewantedad($id) {
        $this->load->database();

        $this->db->set('status', 1);
        $this->db->where('add_id', $id);
        $resp = $this->db->update('all_ads');
        return $resp;
    }

    public function rejectwantedad($id) {
        $this->load->database();

        $this->db->set('status', 2);
        $this->db->where('add_id', $id);
        $resp = $this->db->update('all_ads');
        return $resp;
    }

    public function get_ads_by_paied() {
        $this->load->database();

        $this->db->select('*');
        $this->db->from('all_ads');
        $this->db->where('all_ads.status', 1);
        $this->db->where('all_ads.add_type', 'Technician');
        $this->db->join('user', 'user.contact = all_ads.user_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function save_add($category, $district, $city, $mobile, $fixed, $description, $contact, $paymentType, $amount) {
        $this->load->database();

        $data = array(
            'user_id' => $contact,
            'catogary_id' => $category,
            'location' => $city,
            'contact_1' => $mobile,
            'contact_2' => $fixed,
            'description' => $description,
            'date_created' => date("Y-m-d H:i:s"),
            'district' => $district,
            'icon' => '',
            'add_type' => 'Technician',
            'status' => '0',
            'payment_type' => $paymentType,
            'amount' => $amount
        );
        $responce = $this->db->insert('all_ads', $data);

        return $responce;
    }

}
