<div class="card">
    <div class="card-header">
        <strong class="card-title">Categories</strong>
    </div>
    <div class="card-body">
        <button data-toggle="modal" data-target="#add-category-modal" type="button" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp; Add New</button>
        <br>
        <br>
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Category</th>
                    <th>Icon</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($cat as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->category; ?></td>
                        <td><img src="<?php echo $row->icon; ?>" style="width: 37px;"></td>
                        <td>
                            <button onclick="categoryEdit(<?php echo $row->id; ?>);" data-toggle="modal" data-target="#update-category-modal" type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp; Edit</button>
                            <button onclick="categoryDeletes(<?php echo $row->id; ?>);" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp; Delete</button>
                        </td>
                    </tr>

                    <?php
                    $i ++;
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<!-- add-category-modal -->
<div class="modal fade" id="add-category-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Category</h4>
            </div>
            <div class="modal-body">
                <form id="categorysaveform" action="categories/save" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="category">Category:</label>
                        <input type="text" class="form-control" name="category">
                    </div>
                    <div class="form-group">
                        <label for="myfile">Icon:</label>
                        <input type="file" class="form-control" name="myfile">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Add</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end-add-category-modal -->

<!-- update-category-modal -->
<div class="modal fade" id="update-category-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update New Category</h4>
            </div>
            <div class="modal-body">
                <form action="categories/update" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="category">Category:</label>
                        <input type="text" class="form-control" id="catName" name="category">
                        <input type="text" class="form-control" id="catId" name="catId" hidden>
                    </div>
                    <div class="form-group">
                        <label for="myfile">Icon:</label>
                        <input type="file" class="form-control" name="myfile">
                    </div>
                    <img id="catIcon" src="" style="width: 200px;">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> Update </button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Close </button>
            </div>
        </div>
    </div>
</div>
<!-- end-update-category-modal -->

<script>
    $(document).ready(function () {
        $("#categorysaveform").validate({
            rules: {
                category: {required: true},
                myfile: {required: true},
            }
        });
    });

    function categoryEdit(a) {
        $.ajax({
            url: 'categories/getCategoryById/' + a,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                console.log(data.id);
                $('#catId').val(data.id);
                $('#catName').val(data.category);
                $('#catIcon').attr('src', data.icon);
            }
        });
    }
    function categoryDeletes(a) {
        $.ajax({
            url: 'categories/deleteCategory/' + a,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
              window.location.reload();
            }
        });
    }
</script>