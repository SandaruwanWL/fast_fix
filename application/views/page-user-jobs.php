<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <link rel="stylesheet" href="assets/css/application/wanted.css">

        <script>
            $(document).ready(function () {
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');
            });
        </script>
    </head>

    <style>
        .project-tab {
            padding: 10%;
            margin-top: -8%;
        }
        .project-tab #tabs{
            background: #007b5e;
            color: #eee;
        }
        .project-tab #tabs h6.section-title{
            color: #eee;
        }
        .project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            color: #0062cc;
            background-color: transparent;
            border-color: transparent transparent #f3f3f3;
            border-bottom: 3px solid !important;
            font-size: 16px;
            font-weight: bold;
        }
        .project-tab .nav-link {
            border: 1px solid transparent;
            border-top-left-radius: .25rem;
            border-top-right-radius: .25rem;
            color: #0062cc;
            font-size: 16px;
            font-weight: 600;
        }
        .project-tab .nav-link:hover {
            border: none;
        }
        .project-tab thead{
            background: #f3f3f3;
            color: #333;
        }
        .project-tab a{
            text-decoration: none;
            color: #333;
            font-weight: 600;
        }
    </style>

    <script>
        function setJobId(val) {
            document.getElementById("jbid").value = val;
        }
    </script>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-dashboard'); ?>
            </header>
        </div>
        <div class="container">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <!-- <div class="page-title">
                                    <button data-toggle="modal" data-target="#add-category-modal"  class="btn btn-success" type="button" value="Add New">+ Add New</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-pending" role="tab" aria-controls="nav-home" aria-selected="true">Pending Jobs</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-approved" role="tab" aria-controls="nav-profile" aria-selected="false">Approved Jobs</a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-completed" role="tab" aria-controls="nav-contact" aria-selected="false">Completed Jobs</a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-rejected" role="tab" aria-controls="nav-contact" aria-selected="false">Rejected Jobs</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-pending" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($pendingjobs as $pj) { ?>
                                            <tr>
                                                <td><?php echo $pj->user_id; ?></td>
                                                <td><?php echo $pj->job_title; ?></td>
                                                <td><?php echo $pj->description; ?></td>
                                                <td><?php echo $pj->added_date; ?></td>
                                                <td>
                                                    <button value="<?php echo $pj->id; ?>" onclick="setJobId(this.value)" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myjobModal">Approve</button>
                                                    <form action="rejectjob" method="post">
                                                        <input type="hidden" value="<?php echo $pj->id; ?>" name="jobid">
                                                        <button type="submit" class="btn btn-danger btn-sm">Reject</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-approved" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($approvedjobs as $aj) { ?>
                                            <tr>
                                                <td><?php echo $aj->user_id; ?></td>
                                                <td><?php echo $aj->job_title; ?></td>
                                                <td><?php echo $aj->description; ?></td>
                                                <td><?php echo $aj->added_date; ?></td>
                                                <?php if ($aj->paid == 0) { ?>
                                                    <td>Unpaid</td>
                                                <?php }if ($aj->paid == 1) { ?>
                                                    <td>Paid</td>
                                                <?php } ?>
                                                <td>
                                                    <button class="btn btn-primary btn-sm">View</button>
                                                    <button value="<?php echo $aj->id; ?>" onclick="setJobId(this.value)" data-toggle="modal" data-target="#myjobModal" class="btn btn-success btn-sm">Pay</button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-completed" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($completedjobs as $cj) { ?>
                                            <tr>
                                                <td><?php echo $cj->user_id; ?></td>
                                                <td><?php echo $cj->job_title; ?></td>
                                                <td><?php echo $cj->description; ?></td>
                                                <td><?php echo $cj->added_date; ?></td>
                                                <td>
                                                    <button class="btn btn-primary btn-sm">View</button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-rejected" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rejectedjobs as $rj) { ?>
                                            <tr>
                                                <td><?php echo $rj->user_id; ?></td>
                                                <td><?php echo $rj->job_title; ?></td>
                                                <td><?php echo $rj->description; ?></td>
                                                <td><?php echo $rj->added_date; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>

<div class="modal fade" id="add-category-modal" role="document">
    <div class="modal-dialog modal-lg ">
        <form action="userprofile/saveWantedAds" method="post">

            <div class="modal-content">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="user" value="<?php echo $_SESSION["user"]; ?>">
                                    <div class="form-group">
                                        <label>Topic</label>
                                        <input type="text" class="form-control" name="topic">
                                    </div>
                                    <div class="form-group">
                                        <label>Catogary</label>
                                        <select class="form-control" name="cat">
                                            <?php
                                            foreach ($catogaries as $row) {
                                                ?>
                                                <option value="<?php echo $row->id; ?>" ><?php echo $row->category; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Description</label>
                                        <textarea class="form-control" rows="5" name="des"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pwd">District</label>
                                        <select id="district" class="form-control" name="dis">
                                            <?php
                                            foreach ($districts as $row) {
                                                ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->name_en; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">City:</label>
                                        <select class="form-control" name="city">  
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Phone Number 1:</label>
                                        <input type="text" class="form-control" name="c1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"> Add </button>
                    </form>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

    </div>
</div>

<!-- start job modal -->
<div class="modal fade" id="myjobModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payment Form</h4>
            </div>
            <div class="modal-body">
                <form action="payjob" method="post">
                    <div class="form-group">
                        <label for="usr">Amount</label>
                        <input type="text" class="form-control" name="price" />
                        <input type="hidden" name="jbid" id="jbid">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="reqbtn" class="btn btn-primary">Pay</button>
                <form>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end job modal -->


<script>
    $(document).ready(function () {
        $('#district').on('change', function () {
            var districtID = $(this).val();
            if (districtID) {
                $.ajax({
                    url: 'technicianprofile/changeCitys/' + districtID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="city"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + value.id + '">' + value.name_en + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="city"]').empty();
            }
        });
    });
</script>