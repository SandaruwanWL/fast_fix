
<div class="col-sm-6 col-lg-2">
    <a href="userwantedads">
        <div class="card text-white bg-flat-color-4">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-ticket"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Wanted</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="userworks">
        <div class="card text-white bg-flat-color-3">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-comments-smiley"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">My Works</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="Userprofile">
        <div class="card text-white bg-flat-color-2">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-user"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">My Profile</p>
                </div>
            </div>
        </div>
    </a>
</div>

<div class="col-sm-6 col-lg-2">
    <a href="password-change">
        <div class="card text-white bg-flat-color-4">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-more-alt"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Change Password</p>
                </div>
            </div>
        </div>
    </a>
</div>