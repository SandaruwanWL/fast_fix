<div class="card">
    <div class="card-header">
        <strong class="card-title">Sub Categories</strong>
    </div>
    <div class="card-body">
        <button data-toggle="modal" data-target="#add-category-modal" type="button" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp; Add New</button>
        <br>
        <br>
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cat as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->category; ?></td>
                        <td>
                            <div class="single category">
                                <ul class="list-unstyled">
                                    <?php
                                    $this->load->model('Admin_model');
                                    $subcategory = $this->Admin_model->get_sub_category_by_category_id($row->id);
                                    foreach ($subcategory as $value) {
                                        ?>
                                        <li><?php echo $value->sub_category; ?> 
                                            <a href="#" class="hrefdesable" data-toggle="modal" data-target="#update-category-modal" onclick="subSategoryEdit(<?php echo $value->id; ?>)" > <span class="pull-right" style="padding-left: 17px; color: blue">Update</span></a>
                                            <a href="#" class="hrefdesable" onclick="deleteSubCategory(<?php echo $value->id; ?>)"> <span class="pull-right" style="padding-left: 17px; color: red">Delete</span></a>
        <!--                                            <button type="button" class="pull-right btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp; Edit</button>
                                            <button onclick="deleteSubCategory($row - > id)" type="button" class="pull-right btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp; Delete</button>-->
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>




                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- add-subcategory-modal -->
<div class="modal fade" id="add-category-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Sub Category</h4>
            </div>
            <div class="modal-body">
                <form action="subcategories/save" method="post">
                    <div class="form-group">
                        <label for="usr">Category:</label>
                        <select class="form-control" name="category">
                            <?php
                            foreach ($cat as $row) {
                                ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->category; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="usr">Sub Category:</label>
                        <input type="text" class="form-control" name="subcategory">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Add</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end-add-subcategory-modal -->


<!-- Update-subcategory-modal -->
<div class="modal fade" id="update-category-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="subcategories/update" method="post">
                    <div class="form-group">
                        <label for="usr">Sub Category:</label>
                        <input type="text" class="form-control" name="subcategory">
                        <input hidden type="text" class="form-control" name="subcategoryid">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end-Update-subcategory-modal -->


<script>
    $(document).ready(function () {
        $(".hrefdesable").click(function (event) {
            event.preventDefault();

        });
    });

    function subSategoryEdit(a) {
        $.ajax({
            url: 'subcategories/getSubCategoryById/' + a,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $("input[name='subcategory']").val(data.sub_category);
                $("input[name='subcategoryid']").val(data.id);
            }
        });
    }

    function deleteSubCategory(a) {
        $.ajax({
            url: 'subcategories/deleteSubCategory/' + a,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                window.location.reload();
            }
        });
    }
</script>