<!doctype html>
<html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <?php 
        session_start();
        session_destroy();
        ?>
        <script>

            $(document).ready(function () {

                $("#login").click(function () {
                    var postForm = {
                        'contact': $('#contact').val(),
                        'password': $('#password').val()
                    };

                    $.ajax({
                        type: 'POST',
                        url: 'login/user_login',
                        data: postForm,
                        dataType: 'json',
                        success: function (data)
                        {
                            console.log("data = " + data);
                            if (data == "error") {
                                // $(location).attr('href','login');
                                $('#msg').html('incorrect credentials');
                            } else if (data = "success") {
                                $(location).attr('href', 'dashboard');
                            }
                        },
                        error: function (xhr, desc, err)
                        {
                            console.log("error = " + err);
                        }
                    });
                });

            });
        </script>
    </head>
    <body class="bg-dark">

        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="index.html">
                            <img class="align-content" src="images/logo.png" alt="">
                        </a>
                    </div>
                    <div class="login-form">
                        <form>
                            <div class="form-group">
                                <input type="text" id="contact" class="form-control" placeholder="Contact Number (Ex: 071xxxxxxx)">
                            </div>
                            <div class="form-group">
                                <input type="password" id="password" class="form-control" placeholder="Password">
                            </div>
                            <p id="msg" style="color:red"></p>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                                <label class="pull-right">
                                    <a href="#">Forgotten Password?</a>
                                </label>

                            </div>
                            <button id="login" type="button" class="btn btn-success btn-flat m-b-30 m-t-30">Log in</button>
                            <div class="register-link m-t-15 text-center">
                                <p>Don't have account ? <a href="register"> Register</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
