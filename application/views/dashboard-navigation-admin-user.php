<div class="card-body">
    <ul class="list-group list-group-flush">
        <li class="list-group-item nav-activeUsers">
            <a href="activeusers"> 
                <i class="fa fa-user"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Active Users
            </a>
        </li>
        <li class="list-group-item nav-inactiveUsers">
            <a href="inactiveusers"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Inactive Users
            </a>
        </li>
        <li class="list-group-item nav-bannedUsers">
            <a href="bannedusers"> 
                <i class="fa fa-image"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Banned Users
            </a>
        </li>
    </ul>
</div>