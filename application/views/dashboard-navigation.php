<div class="card-body">
    <ul class="list-group list-group-flush">
        <li class="list-group-item nav-myprofile">
            <a href="technician-profile"> 
                <i class="fa fa-user"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                My Profile
            </a>
        </li>
        <li class="list-group-item nav-myservice">
            <a href="technician-service"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                My Service
            </a>
        </li>
       <li class="list-group-item nav-mywork">
            <a href="technician-work"> 
                <i class="fa fa-image"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                My Work
            </a>
        </li>
        <li class="list-group-item nav-myads">
            <a href="technician-ads"> 
                <i class="fa fa-briefcase"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Ads
            </a>
        </li>
    </ul>
</div>