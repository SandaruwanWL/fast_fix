<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <link rel="stylesheet" href="assets/css/application/wanted.css">

        <script>
            $(document).ready(function () {
                $('#allads').removeClass('btn-secondary');
                $('#allads').addClass('btn-warning');
            });
        </script>

    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-landing'); ?>
            </header>
        </div>

        <div class="container">
            <div class="row">
                <?php
                
                foreach ($wantedAds as $value) {
                    ?>
                    <div class="col-6">
                        <div class="addcard">
                            <div class="card-body">
                                <h5 class="card-title"> <?php echo $value->topic; ?> <button class="btn btn-link btn-sm float-right"><i class="fa fa-map-marker"></i>&nbsp; <?php echo $value->name_en; ?> </button></h5>
                                <hr>
                                <div class="row message-text">
                                    <?php echo $value->description; ?>
                                </div>
                                <div class="media user">
                                    <img class="align-self-center rounded-circle mr-3" src="<?php echo $value->icon; ?>">
                                    <div class="media-body">
                                        <p class="">Posted By <?php echo $value->name; ?></p>
                                        <small class=""><?php echo $value->date_created; ?></small>
                                    </div>
                                    <button type="button" class="btn btn-warning btn-sm float-right"><i class="fa fa-star"></i>&nbsp; <?php echo $value->category; ?></button>
                                    <button type="button" class="btn btn-success btn-sm float-right"><i class="fa fa-phone"></i>&nbsp; <?php echo $value->contact; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
