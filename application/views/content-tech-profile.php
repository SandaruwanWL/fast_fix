<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <form class="row" action="technicianprofile/updateUserDetails" method="post">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile Number:</label>
                                <input disabled type="text" class="form-control" id="mobile" name="mobile">
                                <input type="hidden" name="usermobile" value="<?php echo $_SESSION["user"]; ?>">
                            </div>
                            <div class="form-group">
                                <label for="district">District:</label>
                                <select class="form-control" id="district" name="district">
                                    <?php
                                    foreach ($districts as $row) {
                                        ?>
                                        <option value="<?php echo $row->id; ?>"   <?php echo ($row->id == $districtId) ? 'selected' : ''; ?>   > <?php echo $row->name_en; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="address">Address:</label>
                                <textarea class="form-control" rows="5" id="address" name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fixed">Fixed Line Number:</label>
                                <input type="text" class="form-control" id="fixed" name="fixed">
                            </div>
                            <div class="form-group">
                                <label for="city">City:</label>
                                <select class="form-control" id="city" name="city">

                                    <?php
                                    foreach ($cities as $row) {
                                        ?>
                                        <option value="<?php echo $row->id; ?>"><?php echo $row->name_en; ?></option>
                                    <?php } ?>

                                </select>
                            </div>
                            <br>
                            <button type="submit" id="updatebtn" class="btn btn-primary">Update Details</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        $.ajax({
            url: 'userprofile/getloginUser',
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('input[name="name"]').val(data.name);
                $('input[name="mobile"]').val(data.contact);
                $('input[name="fixed"]').val(data.fixedline);
                $('textarea[name="address"]').val(data.address);
                $('#district').val(data.district_id);
                $('#city').val(data.city_id);
            }
        });

        $('#district').on('change', function () {
            var districtID = $(this).val();
            if (districtID) {
                $.ajax({
                    url: 'technicianprofile/changeCitys/' + districtID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="city"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + value.id + '">' + value.name_en + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="city"]').empty();
            }
        });
    });
</script>