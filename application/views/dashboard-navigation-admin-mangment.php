<div class="card-body">
    <ul class="list-group list-group-flush">
        <li class="list-group-item nav-category">
            <a href="categories"> 
                <i class="fa fa-bars"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Category
            </a>
        </li>
        <li class="list-group-item nav-subcategory">
            <a href="subcategories"> 
                <i class="fa fa-server"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Sub Category
            </a>
        </li>
        <li class="list-group-item nav-print">
            <a href="printAds"> 
                <i class="fa fa-flag"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Payment Reports
            </a>
        </li>
    </ul>
</div>