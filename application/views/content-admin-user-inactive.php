<div class="card">
    <div class="card-header">
        <strong class="card-title">Categories</strong>
    </div>
    <div class="card-body">
    <!-- <button data-toggle="modal" data-target="#add-category-modal" type="button" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp; Add New</button> -->
        <br>
        <br>
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($inactiveusers as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->contact; ?></td>
                        <td>
                            <form action="activateuser" method="post">
                                <button name="user" value="<?php echo $row->contact; ?>" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp; Activate</button>
                            </form>
                            <form action="banuser" method="post">
                                <button name="user" value="<?php echo $row->contact; ?>" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp; Ban</button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>