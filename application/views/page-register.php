<!doctype html>
<html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <script>
            $(document).ready(function () {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(position => {
//                         alert("Location: latitude = " + position.coords.latitude + " , longitude" + position.coords.longitude);
                        $("#longitude").val(position.coords.latitude);
                        $("#latitude").val(position.coords.longitude);
                    });
                } else {
                    console.log("Geolocation is not supported by this browser.");
                }

                $(window).on('popstate', function () {
                    location.reload(true);
                });

                $("#sfuserlogin").hide();
                $("#spuserlogin").hide();
                $('#rasf').prop('disabled', true);
                $('#rasp').prop('disabled', true);

                $("#sf").click(function () {
                    $("#sfuserlogin").toggle("slide");
                    var currentUrl = window.location.href;
                    var parsedUrl = currentUrl += "?role=sf";
                    window.history.pushState({path: parsedUrl}, '', parsedUrl);
                    $("#mainlogin").hide();
                });

                $("#sp").click(function () {
                    $("#spuserlogin").toggle("slide");
                    var currentUrl = window.location.href;
                    var parsedUrl = currentUrl += "?role=sp";
                    window.history.pushState({path: parsedUrl}, '', parsedUrl);
                    $("#mainlogin").hide();
                });

                $("#cpassword").keyup(function () {
                    if ($("#cpassword").val() != $("#password").val()) {
                        $("#msg").html("passwords did not match");
                        $('#rasf').prop('disabled', true);
                    } else if ($("#cpassword").val() == $("#password").val()) {
                        $("#msg").html("");
                        $('#rasf').prop('disabled', false);
                    }
                });

                $("#cpassword_sp").keyup(function () {
                    if ($("#cpassword_sp").val() != $("#password_sp").val()) {
                        $("#msg_sp").html("passwords did not match");
                        $('#rasp').prop('disabled', true);
                    } else if ($("#cpassword_sp").val() == $("#password_sp").val()) {
                        $("#msg_sp").html("");
                        $('#rasp').prop('disabled', false);
                    }
                });

                $("#rasf").click(function () {
                    var postForm = {
                        'name': $('#name').val(),
                        'contact': $('#contact').val(),
                        'password': $('#password').val(),
                        'long': $('#longitude').val(),
                        'lat': $('#latitude').val()
                    };

                    $.ajax({
                        type: 'POST',
                        url: 'register/save_normal_user',
                        data: postForm,
                        dataType: 'json',
                        success: function (data)
                        {
                            console.log("data = " + data);
                            if (data != "error") {
                                $(location).attr('href', 'verify');
                            }
                        },
                        error: function (xhr, desc, err)
                        {
                            console.log("error = " + err);
                        }
                    });
                });

                $("#rasp").click(function () {
                    var postForm = {
                        'name': $('#name_sp').val(),
                        'contact': $('#contact_sp').val(),
                        'password': $('#password_sp').val(),
                        'long': $('#longitude').val(),
                        'lat': $('#latitude').val()
                    };

                    $.ajax({
                        type: 'POST',
                        url: 'register/save_technician',
                        data: postForm,
                        dataType: 'json',
                        success: function (data)
                        {
                            console.log("data = " + data);
                            if (data != "error") {
                                $(location).attr('href', 'verify');
                            }
                        },
                        error: function (xhr, desc, err)
                        {
                            console.log("error = " + err);
                        }
                    });
                });

            });
        </script>
    </head>
    <body class="bg-dark">

        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="index.html">
                            <img class="align-content" src="images/logo.png" alt="">
                        </a>
                    </div>
                    <div class="card" id="mainlogin">
                        <div class="card-body">
                            <h3 style="text-align:center;">Sign Up Free Account</h3>
                            <br><br>
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-md-6" style="text-align:center;">
                                        <div class="card" style="background-color: #e0e0e0;">
                                            <div class="card-body">
                                                <h5>Service Provider</h5><br>
                                                <p>Create professional profile and find jobs to work.</p>
                                                <button type="button" id="sp" class="btn btn-danger">Sign Up</button>
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6" style="text-align:center;">
                                        <div class="card" style="background-color: #e0e0e0;">
                                            <div class="card-body">
                                                <h5>Service Finder</h5><br>
                                                <p>Find Professional Service Providers to do your works.</p>
                                                <button type="button" id="sf" class="btn btn-danger">Sign Up</button>
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="login-form" id="sfuserlogin">
                        <form>
                            <div class="form-group">
                                <input type="text" id="name" class="form-control" placeholder="Name">
                                <input type="hidden" id="longitude" class="form-control">
                                <input type="hidden" id="latitude" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" id="contact" class="form-control" placeholder="Contact Number (Ex: 071xxxxxxx)">
                            </div>
                            <div class="form-group">
                                <input type="password" id="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="password" id="cpassword" class="form-control" placeholder="Confirm Password">
                            </div>
                            <p id="msg" style="color:red;"></p>
                            <button type="button" id="rasf" class="btn btn-primary btn-flat m-b-30 m-t-30">Register as Service Finder</button>
                            <br>
                            <div class="checkbox">
                                <label style="text-align:center;">
                                    By signing up for an account you agree to our Terms and Conditions
                                </label>
                            </div>
                            <div class="register-link m-t-15 text-center">
                                <p>Already have account ? <a href="#"> Log in</a></p>
                            </div>
                        </form>
                    </div>

                    <div class="login-form" id="spuserlogin">
                        <form>
                            <div class="form-group">
                                <input type="text" id="name_sp" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="text" id="contact_sp" class="form-control" placeholder="Contact Number (Ex: 071xxxxxxx)">
                            </div>
                            <div class="form-group">
                                <input type="password" id="password_sp" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="password" id="cpassword_sp" class="form-control" placeholder="Confirm Password">
                            </div>
                            <p id="msg_sp" style="color:red;"></p>
                            <button type="button" id="rasp" class="btn btn-primary btn-flat m-b-30 m-t-30">Register as Service Provider</button>
                            <br>
                            <div class="checkbox">
                                <label style="text-align:center;">
                                    By signing up for an account you agree to our Terms and Conditions
                                </label>
                            </div>
                            <div class="register-link m-t-15 text-center">
                                <p>Already have account ? <a href="#"> Log in</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
