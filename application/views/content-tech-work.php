<style>
.project-tab {
    padding: 10%;
    margin-top: -8%;
}
.project-tab #tabs{
    background: #007b5e;
    color: #eee;
}
.project-tab #tabs h6.section-title{
    color: #eee;
}
.project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #0062cc;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: bold;
}
.project-tab .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #0062cc;
    font-size: 16px;
    font-weight: 600;
}
.project-tab .nav-link:hover {
    border: none;
}
.project-tab thead{
    background: #f3f3f3;
    color: #333;
}
.project-tab a{
    text-decoration: none;
    color: #333;
    font-weight: 600;
}
</style>

<script>
function setJobId(val){
document.getElementById("jbid").value=val;
}
</script>


<h4>My Jobs</h4>
<hr>
<!-- <h4>Upload Images</h4>
<p>Upload images of the work you have done. ( Only JPG, JPEG &amp; PNG image formates are allowed - Max 5 Images )</p> -->
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<!-- start tabs -->

<div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-pending" role="tab" aria-controls="nav-home" aria-selected="true">Pending Jobs</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-approved" role="tab" aria-controls="nav-profile" aria-selected="false">Approved Jobs</a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-completed" role="tab" aria-controls="nav-contact" aria-selected="false">Completed Jobs</a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-rejected" role="tab" aria-controls="nav-contact" aria-selected="false">Rejected Jobs</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-pending" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($pendingjobs as $pj){ ?>
                                        <tr>
                                            <td><?php echo $pj->user_id; ?></td>
                                            <td><?php echo $pj->job_title; ?></td>
                                            <td><?php echo $pj->description; ?></td>
                                            <td><?php echo $pj->added_date; ?></td>
                                            <td>
                                            <button value="<?php echo $pj->id; ?>" onclick="setJobId(this.value)" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myjobModal">Approve</button>
                                            <form action="rejectjob" method="post">
                                            <input type="hidden" value="<?php echo $pj->id; ?>" name="jobid">
                                            <button type="submit" class="btn btn-danger btn-sm">Reject</button>
                                            </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-approved" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($approvedjobs as $aj){ ?>
                                        <tr>
                                            <td><?php echo $aj->user_id; ?></td>
                                            <td><?php echo $aj->job_title; ?></td>
                                            <td><?php echo $aj->description; ?></td>
                                            <td><?php echo $aj->added_date; ?></td>
                                            <?php if($aj->paid==0){?>
                                                <td>Unpaid</td>
                                            <?php }if($aj->paid==1){ ?>
                                                <td>Paid</td>
                                            <?php } ?>
                                            <td>
                                            <button class="btn btn-primary btn-sm">View</button>
                                            <form action="completejob" method="post">
                                            <input type="hidden" value="<?php echo $aj->id; ?>" name="jobid">
                                            <button class="btn btn-success btn-sm">Complete</button>
                                            </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-completed" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($completedjobs as $cj){ ?>
                                        <tr>
                                            <td><?php echo $cj->user_id; ?></td>
                                            <td><?php echo $cj->job_title; ?></td>
                                            <td><?php echo $cj->description; ?></td>
                                            <td><?php echo $cj->added_date; ?></td>
                                            <td>
                                            <button class="btn btn-primary btn-sm">View</button>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-rejected" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Job Title</th>
                                            <th>Description</th>
                                            <th>Added Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($rejectedjobs as $rj){ ?>
                                        <tr>
                                            <td><?php echo $rj->user_id; ?></td>
                                            <td><?php echo $rj->job_title; ?></td>
                                            <td><?php echo $rj->description; ?></td>
                                            <td><?php echo $rj->added_date; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- end tabs -->
    <!-- <div class="row">
        <form action="#" enctype="multipart/form-data" method="post">
                <div class="item-content img_upload_ac" id="img_upload_ac">
                    <label class="custom-file-upload btn btn-info submit">
                        <i class="fas fa-upload"></i>&nbsp;&nbsp;Select Images to Upload
                    </label>
                    <input type="file" name="uploadfiles" id="ajaxuploadbutton" class="vld" style="display:none;" accept=".png, .jpg, .jpeg, .pdf">
                    
                </div>
            <button type="submit" class="btn btn-success submit" id="btn_post" name="btn_post" style="display: none;"><i class="fas fa-save"></i>&nbsp;&nbsp;Post Images</button>
        </form>
    </div> -->
</div>
<!-- <img src="images/category-icons/image.jpg"> -->

<!-- start job modal -->
<div class="modal fade" id="myjobModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enter Service Fee</h4>
        </div>
        <div class="modal-body">
        <form action="approvejob" method="post">
        <div class="form-group">
        <label for="usr">Cost</label>
        <input type="text" class="form-control" name="jobcost" />
        <input type="hidden" name="jbid" id="jbid">
        </div>
        </div>
        <div class="modal-footer">
           <button type="submit" id="reqbtn" class="btn btn-primary">Approve</button>
           <form>
           <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!-- end job modal -->

<script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/init/datatables-init.js"></script>
