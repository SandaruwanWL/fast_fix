<div class="col-sm-6 col-lg-2">
    <a href="activetechnicians">
        <div class="card text-white bg-flat-color-1">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg fa fa-wrench"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Technicians</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="activeusers">
        <div class="card text-white bg-flat-color-6">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg fa fa-users"></i>
                </div>

                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Users</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="categories">
        <div class="card text-white bg-flat-color-3">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg fa fa-pencil-square-o"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Managment</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="approvedtechads">
        <div class="card text-white bg-flat-color-2">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg fa fa-clone"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Ads</p>
                </div>
            </div>
        </div>
    </a>
</div>

<div class="col-sm-6 col-lg-2">
    <a href="password-change">
        <div class="card text-white bg-flat-color-4">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-more-alt"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Change Password</p>
                </div>
            </div>
        </div>
    </a>
</div>