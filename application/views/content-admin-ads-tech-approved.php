<div class="card">
    <div class="card-header">
        <strong class="card-title">Categories</strong>
    </div>
    <div class="card-body">
        <br>
        <br>
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Category</th>
                    <th>District</th>
                    <th>Description</th>
                    <th>Date Created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($approvedtechads as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->add_id; ?></td>
                        <td><?php echo $row->user_id; ?></td>
                        <td><?php echo $row->category; ?></td>
                        <td><?php echo $row->name_en; ?></td>
                        <td><?php echo $row->description; ?></td>
                        <td><?php echo $row->date_created; ?></td>
                        <td>
                            <!--<form action="" method="post">-->
                            <button onclick="setValue(<?php echo $row->add_id; ?>)" data-toggle="modal" data-target="#mediumModal" name="add_id" value="<?php echo $row->add_id; ?>" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp; View</button>
                            <!--</form>-->
                            <form action="rejecttechad" method="post">
                                <button name="add_id" value="<?php echo $row->add_id; ?>" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp; Reject</button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<link rel="stylesheet" href="assets/css/application/wanted.css">
<!-- view-modal -->
<div class="modal fade" id="mediumModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body">
                    <h5 class="card-title" id="topic">.<button class="btn btn-link btn-sm float-right"><i class="fa fa-map-marker"></i>&nbsp; </button></h5>
                    <hr>
                    <div class="row message-text" id="description"></div>
                    <div class="media user">
                        <img class="align-self-center rounded-circle mr-3" id="icon" src="">
                        <div class="media-body">
                            <p id="PostedBy"></p>
                            <small class="">19-07-17</small>
                        </div>
                        <button type="button" class="btn btn-warning btn-sm float-right"><i class="fa fa-star"></i>&nbsp; Constractor</button>
                        <button type="button" class="btn btn-success btn-sm float-right"><i class="fa fa-phone"></i>&nbsp; 0771924931</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view-modal -->

<script>
    function setValue(a) {
        $.ajax({
            url: 'dashboard/getAllAdsById/' + a,
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('#topic').html('<button class="btn btn-link btn-sm"><i class="fa fa-map-marker"></i>&nbsp; '+data.name_en+' </button>');
                $('#description').html(data.description);
                $('#icon').attr('src', data.icon);
                $('#PostedBy').html(data.name);
            }
        });
    }
</script>