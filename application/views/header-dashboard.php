<div class="top-left">
    <div class="navbar-header">
        <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
    </div>
</div>
<div class="top-right">
    <div class="header-menu">
        <div class="header-left">
            <a href="./"><button id="allads" type="button" class="btn btn-secondary btn-sm"><i class="fa fa-address-card-o"></i>&nbsp;  ALL ADS</button></a>
            <a href="wanted"><button id="wanted" type="button" class="btn btn-secondary btn-sm"><i class="fa fa-lightbulb-o"></i>&nbsp;  WANTED</button></a>
            <a href="dashboard"><button id="dashboard" type="button" class="btn btn-secondary btn-sm"><i class="fa fa-dashcube"></i>&nbsp;  DASHBOARD</button></a>
            <a href="logout"><button type="button" class="btn btn-link btn-sm"><i class="fa fa-sign-out"></i>&nbsp; LOG OUT</button></a>
        </div>
    </div>
</div>