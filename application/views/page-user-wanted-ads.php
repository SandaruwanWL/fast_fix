<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <link rel="stylesheet" href="assets/css/application/wanted.css">

        <script>
            $(document).ready(function () {
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');
            });
        </script>
    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-dashboard'); ?>
            </header>
        </div>
        <div class="container">
            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <button data-toggle="modal" data-target="#add-category-modal"  class="btn btn-success" type="button" value="Add New">+ Add New</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                foreach ($ads as $value) {
                    ?>
                    <div class="col-6">
                        <div class="addcard">
                            <div class="card-body">
                                <h5 class="card-title"> <?php echo $value->topic; ?> <button class="btn btn-link btn-sm float-right"><i class="fa fa-map-marker"></i>&nbsp; <?php echo $value->name_en; ?> </button></h5>
                                <hr>
                                <div class="row message-text">
                                    <?php echo $value->description; ?>
                                </div>
                                <div class="media user">
                                    <img class="align-self-center rounded-circle mr-3" src="<?php echo $value->icon; ?>">
                                    <div class="media-body">
                                        <p class="">Posted By <?php echo $value->name; ?></p>
                                        <small class=""><?php echo $value->date_created; ?></small>
                                    </div>
                                    <button type="button" class="btn btn-warning btn-sm float-right"><i class="fa fa-star"></i>&nbsp; <?php echo $value->category; ?></button>
                                    <button type="button" class="btn btn-success btn-sm float-right"><i class="fa fa-phone"></i>&nbsp; <?php echo $value->contact; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>

<div class="modal fade" id="add-category-modal" role="document">
    <div class="modal-dialog modal-lg ">
        <form action="userprofile/saveWantedAds" method="post">

            <div class="modal-content">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="user" value="<?php echo $_SESSION["user"]; ?>">
                                    <div class="form-group">
                                        <label>Topic</label>
                                        <input type="text" class="form-control" name="topic">
                                    </div>
                                    <div class="form-group">
                                        <label>Catogary</label>
                                        <select class="form-control" name="cat">
                                            <?php
                                            foreach ($catogaries as $row) {
                                                ?>
                                                <option value="<?php echo $row->id; ?>" ><?php echo $row->category; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Description</label>
                                        <textarea class="form-control" rows="5" name="des"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pwd">District</label>
                                        <select id="district" class="form-control" name="dis">
                                            <?php
                                            foreach ($districts as $row) {
                                                ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->name_en; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">City:</label>
                                        <select class="form-control" name="city">  
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Phone Number 1:</label>
                                        <input type="text" class="form-control" name="c1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"> Add </button>
                    </form>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $('#district').on('change', function () {
            var districtID = $(this).val();
            if (districtID) {
                $.ajax({
                    url: 'technicianprofile/changeCitys/' + districtID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="city"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + value.id + '">' + value.name_en + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="city"]').empty();
            }
        });
    });
</script>