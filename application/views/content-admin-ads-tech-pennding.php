<div class="card">
    <div class="card-header">
        <strong class="card-title">Categories</strong>
    </div>
    <div class="card-body">
        <br>
        <br>
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Category</th>
                    <th>District</th>
                    <th>Description</th>
                    <th>Date Created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($penndingtechads as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->add_id; ?></td>
                        <td><?php echo $row->user_id; ?></td>
                        <td><?php echo $row->category; ?></td>
                        <td><?php echo $row->name_en; ?></td>
                        <td><?php echo $row->description; ?></td>
                        <td><?php echo $row->date_created; ?></td>
                        <td>
                            <form action="approvetechad" method="post">
                                <button name="add_id" value="<?php echo $row->add_id; ?>" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp; Active</button>
                            </form>
                            <form action="rejecttechad" method="post">
                                <button name="add_id" value="<?php echo $row->add_id; ?>" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp; Reject</button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>