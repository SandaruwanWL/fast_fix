<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>

        <script>
            $(document).ready(function () {
                $('#allads').removeClass('btn-secondary');
                $('#allads').addClass('btn-warning');
            });

            function viewDetalis(id) {
                var postData = {
                    'add': id
                };
                $.ajax({
                    url: 'ad/',
                    type: "post",
                    dataType: "json",
                    data: postData,
                    success: function (data) {
                        $.each(data, function (key, value) {
                            $('#techtp').html(value.contone);
                            $('#techname').html(value.username);
                            $('#techlocation').html(value.citys + ', ' + value.dist);
                            $('input[name="add"]').val(id);

                        });
                    }
                });
            }
        </script>

    </head>

    <body>
        <!-- Right Panel -->
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-landing'); ?>
            </header>
        </div>
        <!-- /#right-panel -->

        <!-- Content -->
        <div class="container">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <?php
                    foreach ($adsab as $row) {
                        ?>
                        <div class="col-md-4">
                            <section class="card">
                                <div class="media" style="margin: 10px 19px;">
                                    <img class="align-self-center mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                                    <div class="media-body">

                                        <!--<form action="ad" method="post" >-->
                                        <button onclick="viewDetalis(<?php echo $row->addid; ?>)" style="padding-left: 0px;" class="btn btn-link" data-toggle="modal" data-target="#mediumModal"><?php echo $row->username; ?></button>
                                        <br>
                                        <i class="fa fa-map-marker"></i>
                                        <label><?php echo $row->dist; ?>,<?php echo $row->citys; ?></label>
                                        <p><?php echo $row->cate; ?></p>

                                                        <!--<input type="hidden"  name="add" value="<?php echo $row->addid; ?>">-->
                                        <!--</form>-->

                                    </div>
                                </div>
                            </section>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->

        <!-- model -->
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" >
                    <div class="card" style="margin-bottom:0px;">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <img class="rounded-circle mx-auto d-block" src="images/admin.jpg" alt="Card image cap">
                            <h5 id="techname" class="text-sm-center mt-2 mb-1"></h5>
                            <div id="techlocation" class="location text-sm-center"><i class="fa fa-map-marker"></i></div>
                        </div>
                        <hr>
                        <div class="card-text text-sm-center">
                            <button id="techtp" type="button" class="btn btn-warning btn-sm"><i class="fa fa-phone"></i>&nbsp; </button>
                        </div>
                    </div>
                    <div class="text-sm-center card-footer">
                        <form action="adpage" method="post" >
                            <button type="submit" class="btn btn-link">View More Details</button>
                            <input type="hidden"  name="add" value="">
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-- model -->

        <div class="clearfix"></div>
        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
