<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
        <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

        <link rel="stylesheet" href="assets/css/lib/chosen/chosen.min.css">

        <style>
            #weatherWidget .currentDesc {
                color: #ffffff!important;
            }
            .traffic-chart {
                min-height: 335px;
            }
            #flotPie1  {
                height: 150px;
            }
            #flotPie1 td {
                padding:3px;
            }
            #flotPie1 table {
                top: 20px!important;
                right: -10px!important;
            }
            .chart-container {
                display: table;
                min-width: 270px ;
                text-align: left;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            #flotLine5  {
                height: 105px;
            }

            #flotBarChart {
                height: 150px;
            }
            #cellPaiChart{
                height: 160px;
            }

        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>

            function selectall() {
                $('.citycheckbox').prop('checked', true);
            }
            function unselectall() {
                $('.citycheckbox').prop('checked', false);
            }

            function districtsSelectorChange() {
                var foo = $('#districtsSelector').val();
                $('#citySelectBody').empty();
                $.each(foo, function (mainkey, value) {
                    $.ajax({
                        url: 'technicianservice/citysBySelectedDistrics/' + value,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $.ajax({
                                url: 'technicianservice/getDistrictById/' + value,
                                type: "GET",
                                dataType: "json",
                                success: function (districtName) {
                                    $.each(districtName, function (key, value) {
                                        $('#citySelectBodyDistricName' + mainkey + '').html(value.name_en);
                                        //this option use >> line excute order is not correctly..!!!!!!!!!!
                                    });
                                }
                            });

                            $('#citySelectBody').append('<h2 id="citySelectBodyDistricName' + mainkey + '"></h2>');
                            $.each(data, function (key, value) {
                                var element = '<label class="checkbox">'
                                        + '<input type="checkbox" class="citycheckbox" name="citycheckbox[]" value="'+ value.id+'" id="inlineCheckbox1">' + value.name_en
                                        + '</label>';
                                $('#citySelectBody').append(element);
                            });
                        }
                    });
                });

            }

            $(document).ready(function () {




                var categoryID = '';
                $('.category').on('click', function (i, obj) {
                    categoryID = $(this).data('id');
                    $.ajax({
                        url: 'technicianservice/loadSubcategory/' + categoryID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('#categoryModelBody').empty();
                            $.each(data, function (key, value) {
                                var element = '<div class="checkbox">'
                                        + '<label for="checkbox1" class="form-check-label ">'
                                        + '<input type="checkbox" id="checkbox1" name="checkbox1" value="' + value.id + '" class="form-check-input">' + value.sub_category + ''
                                        + '</label>'
                                        + '</div>';
                                $('#categoryModelBody').append(element);
                            });
                        }
                    });
                });

                $('#categorySave').on('click', function () {
                    var subCategorys = $(".checkbox input:checkbox:checked").map(function () {
                        return $(this).val();
                    }).get();
                    var postData = {
                        'subCategorys': subCategorys
                    };

                    $.ajax({
                        url: 'technicianservice/saveSubCategory/',
                        type: "POST",
                        dataType: "json",
                        data: postData,
                        success: function (data) {
                            alert(data ? "OK" : "Error");
                            location.reload();
                        }
                    });

                });

                $('#logged_user').hide();
                var postForm = {
                    'contact': $('#logged_user').html()
                };
                $.ajax({
                    type: 'POST',
                    url: 'dashboard/get_user_details',
                    data: postForm,
                    dataType: 'json',
                    success: function (data) {
                        if (data == "error") {
                            alert("error");
                        } else {
                            $('#user_name').html(data.name);
                            if (data.user_type_id == 1) {
                                $('#usertype').html("Admin");
                                $('#mobile').val(data.contact);
                                $('#name').val(data.name);
                                $('#address').val(data.address);
                                $('#dob').val(data.dob);
                                $('#fixed').val(data.fixedline);
                                $('#admin-menu-1').show();
                                $('#admin-menu-2').show();
                                $('#admin-menu-3').show();
                                $('#tech-menu-1').hide();
                                $('#tech-menu-2').hide();
                                $('#tech-menu-3').hide();
                                $('#user-menu-1').hide();
                                $('#user-menu-2').hide();
                                $('#user-menu-3').hide();
                            } else if (data.user_type_id == 2) {
                                $('#usertype').html("Technician");
                                $('#mobile').val(data.contact);
                                $('#name').val(data.name);
                                $('#address').val(data.address);
                                $('#dob').val(data.dob);
                                $('#fixed').val(data.fixedline);
                                $('#admin-menu-1').hide();
                                $('#admin-menu-2').hide();
                                $('#admin-menu-3').hide();
                                $('#user-menu-1').hide();
                                $('#user-menu-2').hide();
                                $('#user-menu-3').hide();
                                $('#tech-menu-1').show();
                                $('#tech-menu-2').show();
                                $('#tech-menu-3').show();
                                $('#tech-menu-4').show();
                            } else if (data.user_type_id == 3) {
                                $('#usertype').html("User");
                                $('#mobile').val(data.contact);
                                $('#name').val(data.name);
                                $('#address').val(data.address);
                                $('#dob').val(data.dob);
                                $('#fixed').val(data.fixedline);
                                $('#admin-menu-1').hide();
                                $('#admin-menu-2').hide();
                                $('#admin-menu-3').hide();
                                $('#tech-menu-1').hide();
                                $('#tech-menu-2').hide();
                                $('#tech-menu-3').hide();
                                $('#user-menu-1').show();
                                $('#user-menu-2').show();
                                $('#user-menu-3').show();
                            }
                        }
                    },
                    error: function (xhr, desc, err)
                    {
                        console.log("error = " + err);
                    }
                });

                $("#updatebtn").click(function () {
                    var postForm = {
                        'name': $('#name').val(),
                        'dob': $('#dob').val(),
                        'mobile': $('#mobile').val(),
                        'fixed': $('#fixed').val(),
                        'district': $('#district').val(),
                        'city': $('#city').val(),
                        'address': $('#address').val()
                    };

                    $.ajax({
                        type: 'POST',
                        url: 'technicianprofile/update_user_details',
                        data: postForm,
                        dataType: 'json',
                        success: function (data) {
                            if (data == "error") {
                                alert("error");
                            } else {
                                $(location).attr('href', 'technician-profile');
                            }
                        },
                        error: function (xhr, desc, err) {
                            console.log("error = " + err);
                        }
                    });
                });
            });
        </script>
    </head>

    <?php
    session_start();
    if (!isset($_SESSION["user"])) {
        header('Location: ' . 'login');
    } else {
        ?>
        <body>
            <!-- Left Panel -->
            <aside id="left-panel" class="left-panel">
                <nav class="navbar navbar-expand-sm navbar-default">
                    <div id="main-menu" class="main-menu collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <h1 id="logged_user"><?php echo $_SESSION["user"]; ?></h1>
                            <li id="user_name"><i class="menu-icon fa fa-user"></i></li>
                            <li class="active">
                                <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                            </li>
                            <li id="usertype" class="menu-title">User Type</li><!-- /.menu-title -->

                            <li id="admin-menu-1" class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Technicians</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-id-badge"></i><a href="activetechnicians">Active Technicians</a></li>
                            <li><i class="fa fa-bars"></i><a href="inactivetechnicians">Inactive Technicians</a></li>
                            <li><i class="fa fa-bars"></i><a href="bannedtechnicians">Banned Technicians</a></li>
                        </ul>
                    </li>

                    <li id="admin-menu-2" class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Users</a>
                        <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-id-badge"></i><a href="activeusers">Active Users</a></li>
                            <li><i class="fa fa-bars"></i><a href="inactiveusers">Inactive Users</a></li>
                            <li><i class="fa fa-bars"></i><a href="bannedusers">Banned Users</a></li>
                        </ul>
                    </li>

                            <li id="admin-menu-3" class="menu-item-has-children dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Management</a>
                                <ul class="sub-menu children dropdown-menu">
                                    <li><i class="menu-icon fa fa-th"></i><a href="categories">Categories</a></li>
                                    <li><i class="menu-icon fa fa-th"></i><a href="subcategories">Sub Categories</a></li>
                                    <li><i class="menu-icon fa fa-th"></i><a href="districts">Districts</a></li>
                                    <li><i class="menu-icon fa fa-th"></i><a href="cities">Cities</a></li>
                                </ul>
                            </li>

                            <li id="user-menu-1" class="menu-item-has-children dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>User Profile</a>
                                <ul class="sub-menu children dropdown-menu">                            <li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Buttons</a></li>
                                    <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Badges</a></li>
                                    <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tabs</a></li>

                                    <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Cards</a></li>
                                    <li><i class="fa fa-exclamation-triangle"></i><a href="ui-alerts.html">Alerts</a></li>
                                    <li><i class="fa fa-spinner"></i><a href="ui-progressbar.html">Progress Bars</a></li>
                                    <li><i class="fa fa-fire"></i><a href="ui-modals.html">Modals</a></li>
                                    <li><i class="fa fa-book"></i><a href="ui-switches.html">Switches</a></li>
                                    <li><i class="fa fa-th"></i><a href="ui-grids.html">Grids</a></li>
                                    <li><i class="fa fa-file-word-o"></i><a href="ui-typgraphy.html">Typography</a></li>
                                </ul>
                            </li>

                            <li id="user-menu-2" class="menu-item-has-children dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>My Works</a>
                                <ul class="sub-menu children dropdown-menu">
                                    <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                                    <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                                </ul>
                            </li>

                            <li id="user-menu-3" class="menu-item-has-children dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>My Technicians</a>
                                <ul class="sub-menu children dropdown-menu">
                                    <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                                    <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                                </ul>
                            </li>

                            <li id="tech-menu-1" class="menu-item-has-children dropdown">
                                <a href="technician-profile" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>My Profile</a>
                                <!-- <ul class="sub-menu children dropdown-menu">                            <li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Buttons</a></li>
                                    <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Badges</a></li>
                                    <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tabs</a></li>
        
                                    <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Cards</a></li>
                                    <li><i class="fa fa-exclamation-triangle"></i><a href="ui-alerts.html">Alerts</a></li>
                                    <li><i class="fa fa-spinner"></i><a href="ui-progressbar.html">Progress Bars</a></li>
                                    <li><i class="fa fa-fire"></i><a href="ui-modals.html">Modals</a></li>
                                    <li><i class="fa fa-book"></i><a href="ui-switches.html">Switches</a></li>
                                    <li><i class="fa fa-th"></i><a href="ui-grids.html">Grids</a></li>
                                    <li><i class="fa fa-file-word-o"></i><a href="ui-typgraphy.html">Typography</a></li>
                                </ul> -->
                            </li>

                            <li id="tech-menu-2" class="menu-item-has-children dropdown">
                                <a href="technician-service" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>My Services</a>
                                <!-- <ul class="sub-menu children dropdown-menu">
                                    <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                                    <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                                </ul> -->
                            </li>

                            <li id="tech-menu-3" class="menu-item-has-children dropdown">
                                <a href="technician-work" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>My Works</a>
                                <!-- <ul class="sub-menu children dropdown-menu">
                                    <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                                    <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                                </ul> -->
                            </li>
                            
                            <li id="tech-menu-4" class="menu-item-has-children dropdown">
                                <a href="#" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Wanted Ads</a>
                                <!-- <ul class="sub-menu children dropdown-menu">
                                    <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                                    <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                                </ul> -->
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </aside>
            <!-- /#left-panel -->
            <!-- Right Panel -->
            <div id="right-panel" class="right-panel">
                <!-- Header-->
                <header id="header" class="header">
                    <div class="top-left">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="./"><img src="../images/logo.png" alt="Logo"></a>
                            <a class="navbar-brand hidden" href="./"><img src="../images/logo2.png" alt="Logo"></a>
                            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </div>
                    <div class="top-right">
                        <div class="header-menu">
                            <div class="header-left">
                                <button class="search-trigger"><i class="fa fa-search"></i></button>
                                <div class="form-inline">
                                    <form class="search-form">
                                        <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                        <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                                    </form>
                                </div>

                                <div class="dropdown for-notification">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell"></i>
                                        <span class="count bg-danger">3</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="notification">
                                        <p class="red">You have 3 Notification</p>
                                        <a class="dropdown-item media" href="#">
                                            <i class="fa fa-check"></i>
                                            <p>Server #1 overloaded.</p>
                                        </a>
                                        <a class="dropdown-item media" href="#">
                                            <i class="fa fa-info"></i>
                                            <p>Server #2 overloaded.</p>
                                        </a>
                                        <a class="dropdown-item media" href="#">
                                            <i class="fa fa-warning"></i>
                                            <p>Server #3 overloaded.</p>
                                        </a>
                                    </div>
                                </div>

                                <div class="dropdown for-message">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-envelope"></i>
                                        <span class="count bg-primary">4</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="message">
                                        <p class="red">You have 4 Mails</p>
                                        <a class="dropdown-item media" href="#">
                                            <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                            <div class="message media-body">
                                                <span class="name float-left">Jonathan Smith</span>
                                                <span class="time float-right">Just now</span>
                                                <p>Hello, this is an example msg</p>
                                            </div>
                                        </a>
                                        <a class="dropdown-item media" href="#">
                                            <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                            <div class="message media-body">
                                                <span class="name float-left">Jack Sanders</span>
                                                <span class="time float-right">5 minutes ago</span>
                                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                                            </div>
                                        </a>
                                        <a class="dropdown-item media" href="#">
                                            <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                            <div class="message media-body">
                                                <span class="name float-left">Cheryl Wheeler</span>
                                                <span class="time float-right">10 minutes ago</span>
                                                <p>Hello, this is an example msg</p>
                                            </div>
                                        </a>
                                        <a class="dropdown-item media" href="#">
                                            <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                            <div class="message media-body">
                                                <span class="name float-left">Rachel Santos</span>
                                                <span class="time float-right">15 minutes ago</span>
                                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="user-area dropdown float-right">
                                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                                </a>

                                <div class="user-menu dropdown-menu">
                                    <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                                    <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                                    <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                                    <a class="nav-link" href="logout"><i class="fa fa-power -off"></i>Logout</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </header>
                <!-- /#header -->
                <!-- Content -->
                <div class="content">
                    <!-- Animated -->
                    <div class="animated fadeIn">

                        <div class="breadcrumbs">
                            <div class="breadcrumbs-inner">
                                <div class="row m-0">
                                    <div class="col-sm-4">
                                        <div class="page-header float-left">
                                            <div class="page-title">
                                                <h1>Technician Dashboard</h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="page-header float-right">
                                            <div class="page-title">
                                                <ol class="breadcrumb text-right">
                                                    <li><a href="#">Technician Dashboard</a></li>
                                                    <li><a href="#">Technician</a></li>
                                                    <li class="active">Service</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content">
                            <div class="animated fadeIn">
                                <h4> Select your categories </h4>
                                <div class="row">
                                    <!-- Saved Caregodies -->
                                    <?php
                                    foreach ($catregory as $row) {
                                        ?>
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header"> <strong>  <?php echo $row->category; ?> </strong> </div>
                                                <div class="card-body">
                                                    <?php
                                                    $this->load->model('Admin_model');
                                                    $loginUser = $this->Admin_model->get_login_user();
                                                    $category = $subCategory = $this->Admin_model->get_subCatregory_by_technician($loginUser['id'], $row->id);
                                                    foreach ($category as $row) {
                                                        ?>
                                                        <label> <?php echo $row->sub_category; ?> </label> </br>

                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>
                                    <!--  Saved Caregodies -->

                                    <?php
                                    foreach ($services as $row) {

                                        $dataTarget = '#smallmodal';
                                        $mainCategory = $this->Admin_model->get_catregory_by_technician($loginUser['id']);

                                        foreach ($mainCategory as $cat) {
                                            if ($row->id == $cat->id) {
                                                $dataTarget = '';
                                                break;
                                            } else {
                                                $dataTarget = '#smallmodal';
                                            }
                                        }
                                        ?>
                                        <div class="col-md-4 category" data-toggle="modal" data-target="<?php echo $dataTarget; ?>" data-id="<?php echo $row->id; ?>">
                                            <aside class="profile-nav alt">
                                                <section class="card">
                                                    <div class="card-header alt">
                                                        <div class="media">
                                                            <img class=" mr-3" style="width:50px; height:50px;" alt="" src="<?php echo $row->icon; ?>">
                                                            <div class="media-body">
                                                                <p class=" display-6"> <?php echo $row->category; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </aside>
                                        </div>
                                    <?php } ?>
                                </div>

                                <form action="technicianservice/saveServiceDetails" method="post">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header"> <strong>  Working Areas </strong> </div>
                                                <div class="card-body">


                                                    <div class="form-group">
                                                        <label for="cc-payment" class="control-label mb-1">Working Districts</label>
                                                        <select data-placeholder="Choose a country..." multiple class="standardSelect" id="districtsSelector" name="districtsSelector[]" onchange="districtsSelectorChange()">
                                                            <option value="" label="default"></option>
                                                            <?php
                                                            foreach ($districts as $row) {
                                                                ?>
                                                                <option value="<?php echo $row->id; ?>">  <?php echo $row->name_en; ?>  </option>
                                                            <?php } ?>  
                                                        </select>
                                                    </div>

                                                    <div id="accordion">
                                                        <div class="card">
                                                            <div class="card-header" id="headingOne">
                                                                <h5 class="mb-0">
                                                                    <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->
                                                                        Working Areas/Cities
                                                                    <!--</button>-->
                                                                </h5>
                                                            </div>

                                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                                <div class="card-body">

                                                                    <!---------------------------------------------------------------------->
                                                                    <div class="control-group" >
                                                                        <div>
                                                                            <button type="button" class="btn btn-warning" onclick="selectall()">Select All</button>
                                                                            <button type="button" class="btn btn-danger" onclick="unselectall()">Unselect All</button>
                                                                        </div>
                                                                        <div id="citySelectBody"></div>
                                                                    </div>
                                                                    <!---------------------------------------------------------------------->

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header"> <strong>  Qualifications </strong> </div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="cc-payment" class="control-label mb-1">Professional Qualifications (Max 300 characters are allowed)</label>
                                                        <textarea name="qualifications" id="textarea-input" rows="3" placeholder="NVQ Level 1, NVQ Level 2" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-header"> <strong>  Working Details </strong> </div>
                                                <div class="card-body">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label for="inputEmail4">Experiance (Years)</label>
                                                            <input type="text" class="form-control" id="inputEmail4" name="experiance" placeholder="ex:3">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputPassword4">Working Hours</label>
                                                            <input type="text" class="form-control" id="inputPassword4" name="workinghours" placeholder="ex: 8.00 am to 5.00 pm">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputPassword4">Best Time to Call</label>
                                                            <input type="text" class="form-control" id="inputPassword4" name="besttimetocall" placeholder="ex: 6.00 pm to 8.00 pm">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="workingdays" class="control-label">Working Days</label>
                                                        <br>
                                                        <span> <label for="wd_monday"> <input type="checkbox" class="rb" name="workingdays[]" id="wd_monday" value="Monday">&nbsp;&nbsp;Monday</label>&nbsp;&nbsp;&nbsp;&nbsp; </span>

                                                        <span> <label for="wd_tuesday">   <input type="checkbox" class="rb" name="workingdays[]" id="wd_tuesday" value="Tuesday">&nbsp;&nbsp;Tuesday</label>&nbsp;&nbsp;&nbsp;</span>

                                                        <span>  <label for="wd_wednesday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_wednesday" value="Wednesday">&nbsp;&nbsp;Wednesday</label>&nbsp;&nbsp;&nbsp;</span>

                                                        <span>  <label for="wd_thursday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_thursday" value="Thursday">&nbsp;&nbsp;Thursday</label>&nbsp;&nbsp;&nbsp;</span>

                                                        <span>  <label for="wd_friday"> <input type="checkbox" class="rb" name="workingdays[]" id="wd_friday" value="Friday">&nbsp;&nbsp;Friday</label>&nbsp;&nbsp;&nbsp; </span>

                                                        <br>
                                                        <span>  <label for="wd_saturday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_saturday" value="Saturday"><span style="color: red;">&nbsp;&nbsp;Saturday</span></label>&nbsp;&nbsp;&nbsp; </span>

                                                        <span>  <label for="wd_sunday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_sunday" value="Sunday"><span style="color: red;">&nbsp;&nbsp;Sunday</span></label><br> </span>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="txtDescription" class="control-label">Description <span class="notif_v">(Max 500 characters are allowed)</span> </label>
                                                        <textarea maxlength="500" id="txtDescription" name="Description" class="form-control description_cls" placeholder="Describe your work"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group btn_grp d-flex justify-content-end">
                                        <input type="submit" class="btn btn-primary" ></input>
                                    </div>
                                </form>

                                <!----- Model ------>
                                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="smallmodalLabel">Small Modal</h5>
                                            </div>
                                            <div class="modal-body">

                                                <div class="form-check" id="categoryModelBody">



                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="categorySave" >Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!----- Model ------>
                            </div><!-- .animated -->
                        </div><!-- .content -->

                        <div class="clearfix"></div>

                        <div class="modal fade none-border" id="event-modal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title"><strong>Add New Event</strong></h4>
                                    </div>
                                    <div class="modal-body"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                        <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /#event-modal -->
                        <!-- Modal - Calendar - Add Category -->
                        <div class="modal fade none-border" id="add-category">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title"><strong>Add a category </strong></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Category Name</label>
                                                    <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Choose Category Color</label>
                                                    <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                                        <option value="success">Success</option>
                                                        <option value="danger">Danger</option>
                                                        <option value="info">Info</option>
                                                        <option value="pink">Pink</option>
                                                        <option value="primary">Primary</option>
                                                        <option value="warning">Warning</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /#add-category -->
                    </div>
                    <!-- .animated -->
                </div>
                <!-- /.content -->
                <div class="clearfix"></div>
                <!-- Footer -->
                <footer class="site-footer">
                    <div class="footer-inner bg-white">
                        <div class="row">
                            <div class="col-sm-6">
                                Copyright &copy; 2018 Ela Admin
                            </div>
                            <div class="col-sm-6 text-right">
                                Designed by <a href="https://colorlib.com">Colorlib</a>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- /.site-footer -->
            </div>
            <!-- /#right-panel -->

            <!-- Scripts -->
            <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
            <script src="assets/js/main.js"></script>

            <!--  Chart js -->
            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

            <!--Chartist Chart-->
            <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
            <script src="assets/js/init/weather-init.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
            <script src="assets/js/init/fullcalendar-init.js"></script>

            <script src="assets/js/lib/chosen/chosen.jquery.min.js"></script>

            <!--Local Stuff-->
            <script>

                                                                                jQuery(document).ready(function () {
                                                                                    jQuery(".standardSelect").chosen({
                                                                                        disable_search_threshold: 10,
                                                                                        no_results_text: "Oops, nothing found!",
                                                                                        width: "100%"
                                                                                    });
                                                                                });

                                                                                jQuery(document).ready(function ($) {


                                                                                    "use strict";

                                                                                    // Pie chart flotPie1
                                                                                    var piedata = [
                                                                                        {label: "Desktop visits", data: [[1, 32]], color: '#5c6bc0'},
                                                                                        {label: "Tab visits", data: [[1, 33]], color: '#ef5350'},
                                                                                        {label: "Mobile visits", data: [[1, 35]], color: '#66bb6a'}
                                                                                    ];

                                                                                    $.plot('#flotPie1', piedata, {
                                                                                        series: {
                                                                                            pie: {
                                                                                                show: true,
                                                                                                radius: 1,
                                                                                                innerRadius: 0.65,
                                                                                                label: {
                                                                                                    show: true,
                                                                                                    radius: 2 / 3,
                                                                                                    threshold: 1
                                                                                                },
                                                                                                stroke: {
                                                                                                    width: 0
                                                                                                }
                                                                                            }
                                                                                        },
                                                                                        grid: {
                                                                                            hoverable: true,
                                                                                            clickable: true
                                                                                        }
                                                                                    });
                                                                                    // Pie chart flotPie1  End
                                                                                    // cellPaiChart
                                                                                    var cellPaiChart = [
                                                                                        {label: "Direct Sell", data: [[1, 65]], color: '#5b83de'},
                                                                                        {label: "Channel Sell", data: [[1, 35]], color: '#00bfa5'}
                                                                                    ];
                                                                                    $.plot('#cellPaiChart', cellPaiChart, {
                                                                                        series: {
                                                                                            pie: {
                                                                                                show: true,
                                                                                                stroke: {
                                                                                                    width: 0
                                                                                                }
                                                                                            }
                                                                                        },
                                                                                        legend: {
                                                                                            show: false
                                                                                        }, grid: {
                                                                                            hoverable: true,
                                                                                            clickable: true
                                                                                        }

                                                                                    });
                                                                                    // cellPaiChart End
                                                                                    // Line Chart  #flotLine5
                                                                                    var newCust = [[0, 3], [1, 5], [2, 4], [3, 7], [4, 9], [5, 3], [6, 6], [7, 4], [8, 10]];

                                                                                    var plot = $.plot($('#flotLine5'), [{
                                                                                            data: newCust,
                                                                                            label: 'New Data Flow',
                                                                                            color: '#fff'
                                                                                        }],
                                                                                            {
                                                                                                series: {
                                                                                                    lines: {
                                                                                                        show: true,
                                                                                                        lineColor: '#fff',
                                                                                                        lineWidth: 2
                                                                                                    },
                                                                                                    points: {
                                                                                                        show: true,
                                                                                                        fill: true,
                                                                                                        fillColor: "#ffffff",
                                                                                                        symbol: "circle",
                                                                                                        radius: 3
                                                                                                    },
                                                                                                    shadowSize: 0
                                                                                                },
                                                                                                points: {
                                                                                                    show: true,
                                                                                                },
                                                                                                legend: {
                                                                                                    show: false
                                                                                                },
                                                                                                grid: {
                                                                                                    show: false
                                                                                                }
                                                                                            });
                                                                                    // Line Chart  #flotLine5 End
                                                                                    // Traffic Chart using chartist
                                                                                    if ($('#traffic-chart').length) {
                                                                                        var chart = new Chartist.Line('#traffic-chart', {
                                                                                            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                                                                                            series: [
                                                                                                [0, 18000, 35000, 25000, 22000, 0],
                                                                                                [0, 33000, 15000, 20000, 15000, 300],
                                                                                                [0, 15000, 28000, 15000, 30000, 5000]
                                                                                            ]
                                                                                        }, {
                                                                                            low: 0,
                                                                                            showArea: true,
                                                                                            showLine: false,
                                                                                            showPoint: false,
                                                                                            fullWidth: true,
                                                                                            axisX: {
                                                                                                showGrid: true
                                                                                            }
                                                                                        });

                                                                                        chart.on('draw', function (data) {
                                                                                            if (data.type === 'line' || data.type === 'area') {
                                                                                                data.element.animate({
                                                                                                    d: {
                                                                                                        begin: 2000 * data.index,
                                                                                                        dur: 2000,
                                                                                                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                                                                                                        to: data.path.clone().stringify(),
                                                                                                        easing: Chartist.Svg.Easing.easeOutQuint
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    // Traffic Chart using chartist End
                                                                                    //Traffic chart chart-js
                                                                                    if ($('#TrafficChart').length) {
                                                                                        var ctx = document.getElementById("TrafficChart");
                                                                                        ctx.height = 150;
                                                                                        var myChart = new Chart(ctx, {
                                                                                            type: 'line',
                                                                                            data: {
                                                                                                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
                                                                                                datasets: [
                                                                                                    {
                                                                                                        label: "Visit",
                                                                                                        borderColor: "rgba(4, 73, 203,.09)",
                                                                                                        borderWidth: "1",
                                                                                                        backgroundColor: "rgba(4, 73, 203,.5)",
                                                                                                        data: [0, 2900, 5000, 3300, 6000, 3250, 0]
                                                                                                    },
                                                                                                    {
                                                                                                        label: "Bounce",
                                                                                                        borderColor: "rgba(245, 23, 66, 0.9)",
                                                                                                        borderWidth: "1",
                                                                                                        backgroundColor: "rgba(245, 23, 66,.5)",
                                                                                                        pointHighlightStroke: "rgba(245, 23, 66,.5)",
                                                                                                        data: [0, 4200, 4500, 1600, 4200, 1500, 4000]
                                                                                                    },
                                                                                                    {
                                                                                                        label: "Targeted",
                                                                                                        borderColor: "rgba(40, 169, 46, 0.9)",
                                                                                                        borderWidth: "1",
                                                                                                        backgroundColor: "rgba(40, 169, 46, .5)",
                                                                                                        pointHighlightStroke: "rgba(40, 169, 46,.5)",
                                                                                                        data: [1000, 5200, 3600, 2600, 4200, 5300, 0]
                                                                                                    }
                                                                                                ]
                                                                                            },
                                                                                            options: {
                                                                                                responsive: true,
                                                                                                tooltips: {
                                                                                                    mode: 'index',
                                                                                                    intersect: false
                                                                                                },
                                                                                                hover: {
                                                                                                    mode: 'nearest',
                                                                                                    intersect: true
                                                                                                }

                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    //Traffic chart chart-js  End
                                                                                    // Bar Chart #flotBarChart
                                                                                    $.plot("#flotBarChart", [{
                                                                                            data: [[0, 18], [2, 8], [4, 5], [6, 13], [8, 5], [10, 7], [12, 4], [14, 6], [16, 15], [18, 9], [20, 17], [22, 7], [24, 4], [26, 9], [28, 11]],
                                                                                            bars: {
                                                                                                show: true,
                                                                                                lineWidth: 0,
                                                                                                fillColor: '#ffffff8a'
                                                                                            }
                                                                                        }], {
                                                                                        grid: {
                                                                                            show: false
                                                                                        }
                                                                                    });
                                                                                    // Bar Chart #flotBarChart End
                                                                                });
            </script>
        </body>
    <?php } ?>
</html>
