<div class="card">
    <div class="card-header">
        <strong class="card-title">Categories</strong>
    </div>
    <div class="card-body">
    <!-- <button data-toggle="modal" data-target="#add-category-modal" type="button" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp; Add New</button> -->
        <br>
        <br>
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Contact</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($bannedusers as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->contact; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>