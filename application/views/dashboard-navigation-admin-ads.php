<div class="card-body">
    <ul class="list-group list-group-flush">
        
        
        <li class="list-group-item nav-pen-tech-ad">
            <a href="penndingtechads"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Pennding Technician Ads
            </a>
        </li>
        <li class="list-group-item nav-pen-wanted-ad">
            <a href="penndingwantedads"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Pennding Wanted Ads
            </a>
        </li>
        
        
        
        <li class="list-group-item nav-appr-tech-ad">
            <a href="approvedtechads"> 
                <i class="fa fa-user"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Approved Technician Ads
            </a>
        </li>
        <li class="list-group-item nav-appr-wanted-ad">
            <a href="approvedwantedads"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Approved Wanted Ads
            </a>
        </li>
        <li class="list-group-item nav-rejc-tech-ad">
            <a href="rejectedtechads"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Rejected Technician Ads
            </a>
        </li>
        <li class="list-group-item nav-rejc-wanted-ad">
            <a href="rejectedwantedads"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Rejected Wanted Ads
            </a>
        </li>
    </ul>
</div>