<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <style>
            a{
                color: #878787;
            }
            .active-link{
                background-color: #ffc300;
                color: black;
            }
            .active-link a{
                color: black;
            }
        </style>
        <script>
            function viewAdd(addID) {
                $.ajax({
                    url: 'technicianads/getAddbyId/' + addID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $.each(data, function (key, value) {
                            $('select[name="category"]').val(value.catogary_id);
                            $('select[name="district"]').val(value.district);
                            $('select[name="city"]').html('<option value="">' + value.location + '</option>');
                            $('input[name="mobile"]').val(value.contact_1);
                            $('input[name="fixed"]').val(value.contact_2);
                            $('textarea[name="description"]').val(value.description);
                            $('#addsubbmintt').hide();
                            $('#addcancel').hide();
                            
                        });
                    }
                });
            }
            
            $(document).ready(function () {
                
                 $('#districts').on('change', function () {
                    var districtID = $(this).val();
                    if (districtID) {
                        $.ajax({
                            url: 'technicianads/changeCitys/' + districtID,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $('select[name="city"]').empty();
                                $.each(data, function (key, value) {
                                    $('select[name="city"]').append('<option value="' + value.id + '">' + value.name_en + '</option>');
                                });
                            }
                        });
                    } else {
                        $('select[name="city"]').empty();
                    }
                });
                
                
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');
                $('.nav-myads').addClass('active-link');
            });
        </script>
    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-dashboard'); ?>
            </header>
        </div>

        <!-- Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-4">
                    <?php $this->load->view('dashboard-navigation'); ?>
                </div>
                <div class="col-lg-8">
                    <div class="card-body">
                        <?php $this->load->view('content-tech-ads'); ?>
                    </div> 
                </div>
            </div>
        </div>
        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
