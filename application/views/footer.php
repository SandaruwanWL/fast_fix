<div class="mt-5 pt-5 footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-xs-12 about-company">
                <img src="images/logo.png" style="width: 339px;">
                <p class="pr-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac ante mollis quam tristique convallis </p>
                <p style="font-size: 21px;"><a href="#"><i class="fa fa-facebook-square mr-1"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-skype"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </p>
            </div>
            <div class="col-lg-3 col-xs-12 links">
                <h4 class="mt-lg-0 mt-sm-3">About us</h4>
                <hr>
                <ul class="m-0 p-0">
                    <li> <a href="#">About us</a></li>
                    <li> <a href="#">Terms & Conditions</a></li>
                    <li> <a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-xs-12 location">
                <h4 class="mt-lg-0 mt-sm-4">Location</h4>
                <hr>
                <p>22, Lorem ipsum dolor, consectetur adipiscing</p>
                <p class="mb-0"><i class="fa fa-phone mr-3"></i>(541) 754-3010</p>
                <p><i class="fa fa-envelope-o mr-3"></i>info@hsdf.com</p>
            </div>
        </div>
        <div class="row mt-5 text-center">
            <div class="col copyright">
                <p class=""><small class="text-white-50">© 2019. All Rights Reserved.</small></p>
            </div>
        </div>
    </div>
</div>