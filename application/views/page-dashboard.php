<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <style>
            .welcomebar{
                padding: 40px 40px;
                color: #747474;
            }
            .col-lg-2{
                min-width: 220px;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(position => {
                        // alert("Location: latitude = " + position.coords.latitude + " , longitude" + position.coords.longitude);
                    });
                } else {
                    console.log("Geolocation is not supported by this browser.");
                }
            });
        </script>
    </head>

    <?php
    session_start();
    if (!isset($_SESSION["user"])) {
        header('Location: ' . 'login');
    } else {
        ?>

        <body>
            <div id="right-panel" class="right-panel">
                <header id="header" class="header">
                    <?php $this->load->view('header-dashboard'); ?>
                </header>
            </div>

            <!-- Content -->
            <div class="container">
                <div class="row welcomebar fl">
                    <div class="col-sm-12 text-center">
                        <div class="page-title">

                            <h1>Hi <?php echo $_SESSION["userObject"]['name']; ?>  <?php echo $_SESSION["userObject"]['user_type_id']; ?></h1>
                            <h3>welcome back..</h3>
                            <?php
                            if (in_array('curl', get_loaded_extensions())) {

                                echo "CURL is available on your web server";
                            } else {
                                echo "CURL is not available on your web server";
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
    <?php
    switch ($_SESSION["userObject"]['user_type_id']) {
        case 1:
            $this->load->view('content-dashboard-admin');
            break;
        case 2:
            $this->load->view('content-dashboard-technician');
            break;
        case 3:
            $this->load->view('content-dashboard-user');
            break;
    }
    ?>
                </div>
            </div>
            <footer >
    <?php $this->load->view('footer'); ?>
            </footer>
        </body>
<?php } ?>
</html>
