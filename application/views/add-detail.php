<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <link rel="stylesheet" href="assets/css/application/user-profile.css">
        <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css"/>

        <script>
        $(document).ready(function () {
                $('#allads').removeClass('btn-secondary');
                $('#allads').addClass('btn-warning');
            });

            function showMsg(){
                var postForm = {
                'tech_id'     : $('#technicianid').val(),
                'u_id'     : $('#userid').val(),
                'jobtitle'     : $('#jobtitle').val(),
                'jobdesc'     : $('#jobdesc').val()
                };

            $.ajax({
            type      : 'POST',
            url       : 'savejob',
            data      : postForm,
            dataType  : 'json',
            success: function (data)
            {
                console.log("data = "+data);
                if(data=="success"){
                alertify.success('Job Request Sent to Technician');
                // $("#reqbtn").attr("data-dismiss","modal");
                }else if(data=="error"){
                alertify.error('Something Went Wrong');
                // $("#reqbtn").attr("data-dismiss","modal");
                }
            },
            error: function (xhr, desc, err)
            {
            console.log("error = "+err);
            }
            });
            $("#reqbtn").attr("data-dismiss","modal");
            }
        </script>

    </head>

    <body>
        <!-- Right Panel -->
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-landing'); ?>
            </header>
        </div>
        <!-- /right-panel -->

        <!-- Content -->
        <div class="container">

            <div class="container emp-profile">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-img">
                                <img src="images/avatar/default.jpg" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="profile-head">
                                <h5><?php echo $adsab[0]->username; ?></h5>
                                <p class="proile-rating"><?php echo $adsab[0]->dist; ?>,    <span><?php echo $adsab[0]->citys; ?></span></p>
                                <h6><i class="fa fa-map-marker"></i>&nbsp; <span>Working Areas</span></h6>
                                <span style="padding-left: 19px;"><?php echo $adsab[0]->citys; ?></span>
                                <hr>
                                <button type="button" class="btn btn-warning"><i class="fa fa-star"></i>&nbsp; <?php echo $adsab[0]->cate; ?></button>
                                <?php
                                session_start();
                                if (isset($_SESSION["user"])) {?>
                                <button data-toggle="modal" data-target="#myjobModal" type="button" class="btn btn-primary"><i class="fa fa-bullhorn"></i>&nbsp; Hire</button>
                                <input type="hidden" id="technicianid" value="<?php echo $adsab[0]->techids; ?>">
                                <input type="hidden" id="userid" value="<?php echo $_SESSION["user"]; ?>">
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-work">
                                <div class="col ext-prof-view-rating text-center">
                                    <h6 class="bass-rating-text">User Rating 6.72%</h6>
                                    <div class="star-ratings-css animated fadeInUp">
                                    </div>
                                </div>
                            </div>
                            <div class="col ext-prof-view-exp text-center">
                                <h5 class="m-0"><i class="fa fa-briefcase"></i>&nbsp;<span>Description</span></h5>
                                <hr>
                                <span class="ty"><?php echo $adsab[0]->desc; ?></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="stat-widget-five">
                                        <div class="stat-icon dib flat-color-1">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="stat-content">
                                            <div class="text-left dib">
                                                <div class="stat-text"><span><?php echo $adsab[0]->contone; ?>,  <?php echo $adsab[0]->conttwo; ?></span></div>
                                                <div class="stat-heading">Best Time to Call</div>
                                                <span class="stat-heading">7.00pm <span>Working Areas</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>          
            </div>



        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>

<!-- start job modal -->
  <div class="modal fade" id="myjobModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Job Request</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
        <label for="usr">Job Title</label>
        <input type="text" class="form-control" id="jobtitle"></textarea>
        </div>
        <div class="form-group">
        <label for="usr">Description</label>
        <textarea class="form-control" rows="5" id="jobdesc"></textarea>
        </div>
        </div>
        <div class="modal-footer">
           <button type="button" id="reqbtn" class="btn btn-primary" onclick="showMsg()">Send a Request</button>
           <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!-- end job modal -->

    </body>
</html>
