<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <style>
            .list-group-item a{
                color: #878787;
            }
            .active-link{
                background-color: #ffc300;
                color: black;
            }
            .active-link a{
                color: black;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');
                $('.nav-pen-wanted-ad').addClass('active-link');
            });
        </script>
    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-dashboard'); ?>
            </header>
        </div>

        <!-- Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-4">
                    <?php $this->load->view('dashboard-navigation-admin-ads'); ?>
                </div>
                <div class="col-lg-8">
                    <div class="card-body">
                        <?php $this->load->view('content-admin-ads-wanted-pennding'); ?>
                    </div> 
                </div>
            </div>
        </div>
        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
