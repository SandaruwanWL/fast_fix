<div class="col-sm-6 col-lg-2">
    <a href="technician-service">
        <div class="card text-white bg-flat-color-1">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-layout-list-thumb"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">List My Service</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="technician-work">
        <div class="card text-white bg-flat-color-6">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-gallery"></i>
                </div>

                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">My Work</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="technician-ads">
        <div class="card text-white bg-flat-color-3">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-comments-smiley"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Ads</p>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-sm-6 col-lg-2">
    <a href="technician-profile">
        <div class="card text-white bg-flat-color-2">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-user"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">My Profile</p>
                </div>
            </div>
        </div>
    </a>
</div>

<div class="col-sm-6 col-lg-2">
    <a href="password-change">
        <div class="card text-white bg-flat-color-4">
            <div class="card-body">
                <div class="text-center">
                    <i class="icon fade-5 icon-lg ti-more-alt"></i>
                </div>
                <div class="card-content pt-1 text-center">
                    <p class="text-light mt-1 m-0">Change Password</p>
                </div>
            </div>
        </div>
    </a>
</div>