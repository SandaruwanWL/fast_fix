<div class="card-body">
    <ul class="list-group list-group-flush">
        <li class="list-group-item nav-activeTech">
            <a href="activetechnicians"> 
                <i class="fa fa-user"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Active Technician
            </a>
        </li>
        <li class="list-group-item nav-inactiveTech">
            <a href="inactivetechnicians"> 
                <i class="fa fa-list-alt"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Inactive Technician
            </a>
        </li>
        <li class="list-group-item nav-bannedTech">
            <a href="bannedtechnicians"> 
                <i class="fa fa-image"></i> 
                <i class="fa fa-caret-right pull-right"></i> 
                Banned Technician
            </a>
        </li>
    </ul>
</div>