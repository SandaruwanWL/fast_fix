<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <style>
            .list-group-item a{
                color: #878787;
            }
            .active-link{
                background-color: #ffc300;
                color: black;
            }
            .active-link a{
                color: black;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');
                $('.nav-pen-tech-ad').addClass('active-link');
            });
        </script>
    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-dashboard'); ?>
            </header>
        </div>

        <!-- Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Password Change</strong>
                        </div>
                        <div class="card-body">
                            <!-- Credit Card -->
                            <div id="pay-invoice">
                                <div class="card-body">

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <label class="control-label mb-1">Current Password</label>
                                            <input name="pw" type="text" class="form-control">
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="control-label mb-1">New Password</label>
                                            <input name="new" type="text" class="form-control cc-name valid">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Conform Password</label>
                                            <input name="confm" type="text" class="form-control cc-number identified visa">
                                        </div>
                                        <div>
                                            <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">change</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div> <!-- .card -->

                </div>
            </div>
        </div>
        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
