<h4> Select your categories </h4>
<div class="row">
    <!-- Saved Caregodies -->
    <?php
    foreach ($catregory as $row) {
        ?>
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"> <strong>  <?php echo $row->category; ?> </strong> </div>
                <div class="card-body">
                    <?php
                    $this->load->model('Admin_model');
                    $category = $this->Admin_model->get_subCatregory_by_technician($_SESSION["userObject"]['id'], $row->id);
                    foreach ($category as $row) {
                        ?>
                        <label> <?php echo $row->sub_category; ?> </label> </br>
                    <?php } ?>
                </div>
            </div>
        </div>

    <?php } ?>
    <!--  Saved Caregodies -->
    <?php
    foreach ($services as $row) {

        $dataTarget = '#smallmodal';
        $mainCategory = $this->Admin_model->get_catregory_by_technician($_SESSION["userObject"]['id']);

        foreach ($mainCategory as $cat) {
            if ($row->id == $cat->id) {
                $dataTarget = '';
                break;
            } else {
                $dataTarget = '#smallmodal';
            }
        }
        ?>
        <div class="col-md-4 category" data-toggle="modal" data-target="<?php echo $dataTarget; ?>" data-id="<?php echo $row->id; ?>">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header alt">
                        <div class="media">
                            <img class=" mr-3" style="width:50px; height:50px;" alt="" src="<?php echo $row->icon; ?>">
                            <div class="media-body">
                                <p class=" display-6"> <?php echo $row->category; ?> </p>
                            </div>
                        </div>
                    </div>
                </section>
            </aside>
        </div>
    <?php } ?>
</div>

<form action="technicianservice/saveServiceDetails" method="post">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"> <strong>  Working Areas </strong> </div>
                <div class="card-body">

                    <div class="form-group">
                        <label for="cc-payment" class="control-label mb-1">Working Districts</label>
                        <select class="form-control" id="districtsSelector" name="districtsSelector" onchange="districtsSelectorChange()">
                            <option value="" label="Choose a country..."></option>
                            <?php
                            foreach ($districts as $row) {
                                ?>
                                <option  <?php echo $district[0]->id == $row->id ? 'selected' : ''; ?> value="<?php echo $row->id; ?>">  <?php echo $row->name_en; ?>  </option>
                            <?php } ?>  
                        </select>
                    </div>

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    Working Areas/Cities
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">

                                    <!---------------------------------------------------------------------->
                                    <div class="control-group" >
                                        <div>
                                            <button type="button" class="btn btn-warning" onclick="selectall()">Select All</button>
                                            <button type="button" class="btn btn-danger" onclick="unselectall()">Unselect All</button>
                                        </div>
                                        <div id="citySelectBody">
                                            <?php
                                            foreach ($registedworkingcitis as $row) {
                                                ?>
                                                <label class="checkbox">
                                                    <input checked type="checkbox" class="citycheckbox" name="citycheckbox[]" value="' + value.id + '" id="inlineCheckbox1"><?php echo $row->name_en; ?>
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!---------------------------------------------------------------------->

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"> <strong>  Qualifications </strong> </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="cc-payment" class="control-label mb-1">Professional Qualifications (Max 300 characters are allowed)</label>
                        <textarea name="qualifications" id="textarea-input" rows="3" placeholder="NVQ Level 1, NVQ Level 2" class="form-control"> <?php echo empty($savedserviceotherinfo)?'':$savedserviceotherinfo[0]->qualifications;?> </textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"> <strong>  Working Details </strong> </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputEmail4">Experiance (Years)</label>
                            <input type="text" class="form-control" id="inputEmail4" name="experiance" placeholder="ex:3" value="<?php echo empty($savedserviceotherinfo)?'':$savedserviceotherinfo[0]->experiance;?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Working Hours</label>
                            <input type="text" class="form-control" id="inputPassword4" name="workinghours" placeholder="ex: 8.00 am to 5.00 pm" value="<?php echo empty($savedserviceotherinfo)?'':$savedserviceotherinfo[0]->working_hours;?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Best Time to Call</label>
                            <input type="text" class="form-control" id="inputPassword4" name="besttimetocall" placeholder="ex: 6.00 pm to 8.00 pm" value="<?php echo empty($savedserviceotherinfo)?'':$savedserviceotherinfo[0]->best_time_to_call;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="workingdays" class="control-label">Working Days</label>
                        <br>
                        <span> <label for="wd_monday"> <input type="checkbox" class="rb" name="workingdays[]" id="wd_monday" value="Monday">&nbsp;&nbsp;Monday</label>&nbsp;&nbsp;&nbsp;&nbsp; </span>
                        <span> <label for="wd_tuesday">   <input type="checkbox" class="rb" name="workingdays[]" id="wd_tuesday" value="Tuesday">&nbsp;&nbsp;Tuesday</label>&nbsp;&nbsp;&nbsp;</span>
                        <span>  <label for="wd_wednesday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_wednesday" value="Wednesday">&nbsp;&nbsp;Wednesday</label>&nbsp;&nbsp;&nbsp;</span>
                        <span>  <label for="wd_thursday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_thursday" value="Thursday">&nbsp;&nbsp;Thursday</label>&nbsp;&nbsp;&nbsp;</span>
                        <span>  <label for="wd_friday"> <input type="checkbox" class="rb" name="workingdays[]" id="wd_friday" value="Friday">&nbsp;&nbsp;Friday</label>&nbsp;&nbsp;&nbsp; </span>
                        <br>
                        <span>  <label for="wd_saturday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_saturday" value="Saturday"><span style="color: red;">&nbsp;&nbsp;Saturday</span></label>&nbsp;&nbsp;&nbsp; </span>
                        <span>  <label for="wd_sunday">  <input type="checkbox" class="rb" name="workingdays[]" id="wd_sunday" value="Sunday"><span style="color: red;">&nbsp;&nbsp;Sunday</span></label><br> </span>
                    </div>
                    <div class="form-group">
                        <label for="txtDescription" class="control-label">Description <span class="notif_v">(Max 500 characters are allowed)</span> </label>
                        <textarea maxlength="500" id="txtDescription" name="Description" class="form-control description_cls" placeholder="Describe your work"><?php echo empty($savedserviceotherinfo)?'':$savedserviceotherinfo[0]->Description;?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group btn_grp d-flex justify-content-end">
        <input type="<?php echo empty($savedserviceotherinfo)?'submit':'button';?>" value="<?php echo empty($savedserviceotherinfo)?'Save':'Update';?>" class="btn btn-primary" ></input>
    </div>
</form>

<!----- Model ------>
<div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="smallmodalLabel"></h5>
            </div>
            <div class="modal-body">
                <div class="form-check" id="categoryModelBody"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="categorySave" >Confirm</button>
            </div>
        </div>
    </div>
</div>
<!----- Model ------>