<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <link rel="stylesheet" href="assets/css/application/wanted.css">

        <script>
            $(document).ready(function () {
                $('#dashboard').removeClass('btn-secondary');
                $('#dashboard').addClass('btn-warning');
            });
        </script>
    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-dashboard'); ?>
            </header>
        </div>
        <div class="container"> 
            <div class="row">

                <form class="row" action="userprofile/updateUserProfile" method="post">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="hidden" name="user" value="<?php echo $_SESSION["user"]; ?>">
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Address</label>
                            <textarea class="form-control" rows="5" name="address"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pwd">District</label>
                            <select id="district" class="form-control" name="district">
                                <?php
                                foreach ($districts as $row) {
                                    ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->name_en; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pwd">City:</label>
                            <select id="city" class="form-control" name="city"> 
                                <?php
                                foreach ($cities as $row) {
                                    ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->name_en; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary"> Update </button>
                    </div>
                </form>
            </div>   
        </div>

        <footer>
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>


<script>
    $(document).ready(function () {

        $.ajax({
            url: 'userprofile/getloginUser',
            type: "GET",
            dataType: "json",
            success: function (data) {
              console.log(data);
              $('input[name="name"]').val(data.name);
              $('textarea[name="address"]').val(data.address);
              $('#district').val(data.district_id);
              $('#city').val(data.city_id);
            }
        });



        $('#district').on('change', function () {
            var districtID = $(this).val();
            if (districtID) {
                $.ajax({
                    url: 'technicianprofile/changeCitys/' + districtID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="city"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + value.id + '">' + value.name_en + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="city"]').empty();
            }
        });
    });
</script>