<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"> 
                <h4>Ads</h4>
                <button type="button" class="btn btn-success mb-1 pull-right" data-toggle="modal" data-target="#mediumModal">Add New</button>
            </div>
            <div class="card-body">
                <div class="table-stats order-table ov-h">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Location</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($ads as $row) {
                                ?>
                                <tr>
                                    <td> <?php echo $row->user_id; ?> </td>
                                    <td>  <span> <?php echo $row->description; ?> </span> </td>
                                    <td> <span> <?php echo $row->catogary_id; ?> </span> </td>
                                    <td><span> <?php echo $row->location; ?> </span></td>
                                    <td>
                                        <button class="btn btn-secondary" onclick="viewAdd(<?php echo $row->add_id; ?>)" data-toggle="modal" data-target="#mediumModal2" >View</button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!----- Model ------>
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Add new</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="card">
                    <div class="card-body card-block">
                        <form action="techaddsave" method="post" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3"><label class=" form-control-label">ID:</label></div>
                                <div class="col-12 col-md-9">
                                    <p class="form-control-static"><?php echo $loginUser['contact']; ?></p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="select" class=" form-control-label">Category</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="category" id="category" class="form-control">
                                        <?php
                                        foreach ($categorys as $row) {
                                            ?>
                                            <option value="<?php echo $row->id; ?>"> <?php echo $row->category; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3"><label for="selectLg" class=" form-control-label">Distric</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="district" id="districts" class="form-control">
                                        <option value=""> - Please select - </option>
                                        <?php
                                        print_r($districts);
                                        foreach ($districts as $row) {
                                            ?>
                                            <option value="<?php echo $row->id; ?>"> <?php echo $row->name_en; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3"><label for="selectLg" class=" form-control-label">Location</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="city" id="city" class="form-control">
                                        <option value="">Please select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Mobile</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="text-input" name="mobile" placeholder="Mobile" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Fixd line</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="text-input" name="fixed" placeholder="Fixd line" class="form-control"></div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Description</label></div>
                                <div class="col-12 col-md-9"><textarea name="description" id="textarea-input" rows="3" placeholder="Description..." class="form-control"></textarea></div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3"><label for="selectLg" class=" form-control-label">Payment Type</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="cash" id="cash" class="form-control">
                                        <option value="cash">Cash</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Amount (LKR)</label></div>
                                <div class="col-12 col-md-9"><input disabled value="250" type="text" id="amount" name="amount" placeholder="Mobile" class="form-control"></div>
                            </div>

                            <div class="">
                                <button id="addsubbmintt" type="submit" class="btn btn-primary btn-sm pull-right">
                                    <i class="fa fa-dot-circle-o"></i> 
                                    Next
                                </button>
                                <button id="addcancel" type="reset" class="btn btn-danger btn-sm pull-right" style="margin-right: 7px;"> 
                                    <i class="fa fa-ban"></i> Cancel
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!----- Model ------>



<div class="modal fade" id="mediumModal2" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">ID:</label></div>
                            <div class="col-12 col-md-9">
                                <p class="form-control-static"><?php echo $loginUser['contact']; ?></p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="select" class=" form-control-label">Category</label></div>
                            <div class="col-12 col-md-9">
                                <select name="view-category" id="category" class="form-control">
                                    <?php
                                    foreach ($categorys as $row) {
                                        ?>
                                        <option value="<?php echo $row->id; ?>"> <?php echo $row->category; ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3"><label for="selectLg" class=" form-control-label">Distric</label></div>
                            <div class="col-12 col-md-9">
                                <select name="district" id="districts" class="form-control">
                                    <option value=""> - Please select - </option>
                                    <?php
                                    print_r($districts);
                                    foreach ($districts as $row) {
                                        ?>
                                        <option value="<?php echo $row->id; ?>"> <?php echo $row->name_en; ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3"><label for="selectLg" class=" form-control-label">Location</label></div>
                            <div class="col-12 col-md-9">
                                <select name="city" id="city" class="form-control">
                                    <option value="">Please select</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Mobile</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="mobile" placeholder="Mobile" class="form-control"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Fixd line</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="fixed" placeholder="Fixd line" class="form-control"></div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Description</label></div>
                            <div class="col-12 col-md-9"><textarea name="description" id="textarea-input" rows="3" placeholder="Description..." class="form-control"></textarea></div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3"><label for="selectLg" class=" form-control-label">Payment Type</label></div>
                            <div class="col-12 col-md-9">
                                <select name="cash" id="cash" class="form-control">
                                    <option value="cash">Cash</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Amount (LKR)</label></div>
                            <div class="col-12 col-md-9"><input disabled value="250" type="text" id="amount" name="amount" placeholder="Mobile" class="form-control"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function viewAdd(a) {
        $.ajax({
            url: 'technicianprofile/getAdById/' + a,
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log(data);
//                $('input[name="name"]').val(data.name);
//                $('input[name="mobile"]').val(data.contact);
//                $('input[name="fixed"]').val(data.fixedline);
//                $('textarea[name="address"]').val(data.address);
//                $('#district').val(data.district_id);
//                $('#city').val(data.city_id);
            }
        });
    }


    $(document).ready(function () {

    $('#district').on('change', function () {
    $.ajax({
    url: 'userprofile/getloginUser',
            type: "GET",
            dataType: "json",
            success: function (data) {
            $('input[name="name"]').val(data.name);
                    $('input[name="mobile"]').val(data.contact);
                    $('input[name="fixed"]').val(data.fixedline);
                    $('textarea[name="address"]').val(data.address);
                    $('#district').val(data.district_id);
                    $('#city').val(data.city_id);
            }
    });
    });

    $('#district').on('change', function () {
    var districtID = $(this).val();
            if (districtID) {
    $.ajax({
    url: 'technicianprofile/changeCitys/' + districtID,
            type: "GET",
            dataType: "json",
            success: function (data) {
            $('select[name="city"]').empty();
                    $.each(data, function (key, value) {
                    $('select[name="city"]').append('<option value="' + value.id + '">' + value.name_en + '</option>');
                    });
            }
    });
    } else {
    $('select[name="city"]').empty();
    }
    });
    }
    );
</script>