<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <style>
            .card{
                background-color: #e0e0e0;
            }
            body {
                margin-top: 20px;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#allads').removeClass('btn-secondary');
                $('#allads').addClass('btn-warning');
                window.print();
            });
        </script>

    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <div class="top-left">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>

                    </div>

                </div>

            </header>
        </div>

        <!-- Content -->
        <div class="container">
            <div class="container">
                <div class="row ">
                    <div class="well col-xs-12 col-sm-12 col-md-12 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">

                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                <p>
                                    <em><?php echo date("Y/m/d") . "<br>"; ?></em>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-center" style="padding-bottom: 32px;">
                                <h1>Payment Ads Report</h1>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>description</th>
                                        <th class="">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($paiedAds as $value) {
                                    ?>
                                    <tr>
                                        <td class=""><?php echo $value->name; ?>    </td>
                                        <td class="col-md-10"> <?php echo $value->description; ?>  </td>
                                        <td class="col-md-4 ">LKR_250.00</td>
                                    </tr>
                                    
                                    <?php }?>
                                    <tr>
                                        <td>   </td>
                                        <td class="pull-right"><h4><strong>Total: </strong></h4></td>
                                        <td class="text-danger"><h4><strong > <?php echo 'LKR ' . (count($paiedAds) * 250).'.00'; ?></strong></h4></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>
