<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ela Admin - HTML5 Admin Template</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $this->load->view('links'); ?>
        <style>
            .card{
                background-color: #e0e0e0;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#allads').removeClass('btn-secondary');
                $('#allads').addClass('btn-warning');
            });
        </script>

    </head>

    <body>
        <div id="right-panel" class="right-panel">
            <header id="header" class="header">
                <?php $this->load->view('header-landing'); ?>
            </header>
        </div>

        <!-- Content -->
        <div class="container">
            <div class="row">
                <?php
                foreach ($cat as $row) {
                    ?>
                    <div class="col-md-2">
                        <form action="categoryads" method="post" >
                            <div class="card text-center">
                                <div class="card-body">
                                    <div class="mx-auto d-block">
                                        <img class="mx-auto d-block category-icon-home-view " src="<?php echo $row->icon; ?>" >
                                        <button type="submit" class="btn btn-link"><?php echo $row->category; ?></button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden"  name="catid" value="<?php echo $row->id; ?>">
                        </form>
                    </div> 
                <?php } ?>
            </div>
        </div>
        <footer >
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
