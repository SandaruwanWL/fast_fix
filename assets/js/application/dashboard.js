  $(document).ready(function(){

        if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
        // alert("Location: latitude = " + position.coords.latitude + " , longitude" + position.coords.longitude);
        });
        } else {
        console.log("Geolocation is not supported by this browser.");
        }

    $('#logged_user').hide();

        var postForm = {
            'contact'     : $('#logged_user').html()
        };

        $.ajax({
            type      : 'POST',
            url       : 'dashboard/get_user_details',
            data      : postForm,
            dataType  : 'json',
            success: function (data)
            {
                console.log("data = "+data);
                if(data=="error"){
                    alert("error");
                }else{
                    $('#user_name').html(data.name);
                    if(data.user_type_id==1){
                    $('#usertype').html("Admin");
                    $('#admin-menu-1').show();
                    $('#admin-menu-2').show();
                    $('#admin-menu-3').show();
                    $('#tech-menu-1').hide();
                    $('#tech-menu-2').hide();
                    $('#tech-menu-3').hide();
                    $('#user-menu-1').hide();
                    $('#user-menu-2').hide();
                    $('#user-menu-3').hide();
                    }else if(data.user_type_id==2){
                    $('#usertype').html("Technician");
                    $('#admin-menu-1').hide();
                    $('#admin-menu-2').hide();
                    $('#admin-menu-3').hide();
                    $('#user-menu-1').hide();
                    $('#user-menu-2').hide();
                    $('#user-menu-3').hide();
                    $('#tech-menu-1').show();
                    $('#tech-menu-2').show();
                    $('#tech-menu-3').show();
                    $('#tech-menu-4').show();
                    }else if(data.user_type_id==3){
                    $('#usertype').html("User");
                    $('#admin-menu-1').hide();
                    $('#admin-menu-2').hide();
                    $('#admin-menu-3').hide();
                    $('#tech-menu-1').hide();
                    $('#tech-menu-2').hide();
                    $('#tech-menu-3').hide();
                    $('#user-menu-1').show();
                    $('#user-menu-2').show();
                    $('#user-menu-3').show();
                    }
                }
            },
            error: function (xhr, desc, err)
            {
            console.log("error = "+err);
            }
        });
        });