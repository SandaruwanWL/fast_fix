function selectall() {
    $('.citycheckbox').prop('checked', true);
}
function unselectall() {
    $('.citycheckbox').prop('checked', false);
}

function districtsSelectorChange() {
    var district = $('#districtsSelector').val();
    $('#citySelectBody').empty();

    $.ajax({
        url: 'technicianservice/citysBySelectedDistrics/' + district,
        type: "GET",
        dataType: "json",
        success: function (data) {
            $.each(data, function (key, value) {
                var element = '<label class="checkbox">'
                        + '<input type="checkbox" class="citycheckbox" name="citycheckbox[]" value="' + value.id + '" id="inlineCheckbox1">' + value.name_en
                        + '</label>';
                $('#citySelectBody').append(element);
            });
        }
    });
}

$(document).ready(function () {
    var categoryID = '';
    $('.category').on('click', function (i, obj) {
        categoryID = $(this).data('id');
        $.ajax({
            url: 'technicianservice/loadSubcategory/' + categoryID,
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('#categoryModelBody').empty();
                $.each(data, function (key, value) {
                    var element = '<div class="checkbox">'
                            + '<label for="checkbox1" class="form-check-label ">'
                            + '<input type="checkbox" id="checkbox1" name="checkbox1" value="' + value.id + '" class="form-check-input">' + value.sub_category + ''
                            + '</label>'
                            + '</div>';
                    $('#categoryModelBody').append(element);
                    $('#smallmodalLabel').html(value.category);
                });
            }
        });
    });

    $('#categorySave').on('click', function () {
        var subCategorys = $(".checkbox input:checkbox:checked").map(function () {
            return $(this).val();
        }).get();
        var postData = {
            'subCategorys': subCategorys
        };
        console.log(postData);
        if (subCategorys.length > 0) {
            $.ajax({
                url: 'technicianservice/saveSubCategory/',
                type: "POST",
                dataType: "json",
                data: postData,
                success: function (data) {
//                    alert(data ? "OK" : "Error");
                    location.reload();
                }
            });
        } else {
            alert("not selected sub category");
        }


    });
});